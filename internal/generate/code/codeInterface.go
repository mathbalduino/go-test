package code

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile/goImports"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-test/internal/generate/code/templates"
	"go/types"
)

func codeInterface(iTypeName *types.TypeName, fileImports *goImports.GoImports, builtTypes map[string]bool, log *logCLI.LogCLI) generatedCode {
	iType := iTypeName.Type().Underlying().(*types.Interface)
	if iType.NumMethods() == 0 {
		log.Debug("Doesn't contain any methods (skipping it)...")
		return generatedCode{}
	}

	mockFnImportAlias := fileImports.AliasFromPath(mockFnPackagePath)
	methods := make([]templates.Method, iType.NumMethods())
	comparatorsCode := ""
	mocksFnsCode := ""
	for i := 0; i < iType.NumMethods(); i++ {
		currMethod := iType.Method(i)
		methodLog := log.Debug("Analysing method '%s'...", currMethod.Name())
		methodTypeIdentifier := resolveTypeIdentifier(currMethod, fileImports, methodLog)
		signature := currMethod.Type().(*types.Signature)

		params, comparators := codeParams(signature, fileImports, builtTypes, methodLog)
		results := codeResults(signature, fileImports, builtTypes, methodLog)
		methods[i] = templates.Method{
			Name:           currMethod.Name(),
			TypeIdentifier: methodTypeIdentifier,
			Params:         params,
			Results:        results,
		}

		comparatorsCode += comparators
		_, alreadyBuilt := builtTypes[methodTypeIdentifier]
		if alreadyBuilt {
			continue
		}

		mockFnCode, e := templates.MockFn(methodTypeIdentifier, mockFnImportAlias, params, results)
		if e != nil {
			methodLog.Fatal("%v", e)
		}
		methodLog.Debug("%s code generated", templates.MockFnTypeName)

		mockedFnCode, e := templates.MockedFn(methodTypeIdentifier, mockFnImportAlias, params, results)
		if e != nil {
			methodLog.Fatal("%v", e)
		}
		methodLog.Debug("%s code generated", templates.MockedFnTypeName)

		mockFnCallCode, e := templates.MockFnCall(methodTypeIdentifier, mockFnImportAlias, params)
		if e != nil {
			methodLog.Fatal("%v", e)
		}
		methodLog.Debug("%s code generated", templates.MockFnCallTypeName)

		mockFnCallsCode, e := templates.MockFnCalls(methodTypeIdentifier, mockFnImportAlias)
		if e != nil {
			methodLog.Fatal("%v", e)
		}
		methodLog.Debug("%s code generated", templates.MockFnCallsTypeName)

		mockFnMultiCode, e := templates.MockFnMulti(methodTypeIdentifier, mockFnImportAlias, params, results)
		if e != nil {
			methodLog.Fatal("%v", e)
		}
		methodLog.Debug("%s code generated", templates.MockFnMultiTypeName)

		mockFnMultiCallCode, e := templates.MockFnMultiCall(methodTypeIdentifier, mockFnImportAlias, params)
		if e != nil {
			methodLog.Fatal("%v", e)
		}
		methodLog.Debug("%s code generated", templates.MockFnMultiCallTypeName)

		mockFnStubCode, e := templates.MockFnStub(methodTypeIdentifier, mockFnImportAlias)
		if e != nil {
			methodLog.Fatal("%v", e)
		}
		methodLog.Debug("%s code generated", templates.MockFnStubTypeName)

		mocksFnsCode += mockFnCode + "\n"
		mocksFnsCode += mockedFnCode + "\n"
		mocksFnsCode += mockFnCallCode + "\n"
		mocksFnsCode += mockFnCallsCode + "\n"
		mocksFnsCode += mockFnMultiCode + "\n"
		mocksFnsCode += mockFnMultiCallCode + "\n"
		mocksFnsCode += mockFnStubCode + "\n"
		builtTypes[methodTypeIdentifier] = true

		if len(results) != 0 {
			mockFnRetCallCode, e := templates.MockFnRetCall(methodTypeIdentifier, mockFnImportAlias, results)
			if e != nil {
				methodLog.Fatal("%v", e)
			}
			methodLog.Debug("%s code generated", templates.MockFnRetCallTypeName)

			mockFnRetMultiCallCode, e := templates.MockFnRetMultiCall(methodTypeIdentifier, mockFnImportAlias, results)
			if e != nil {
				methodLog.Fatal("%v", e)
			}
			methodLog.Debug("%s code generated", templates.MockFnRetMultiCallTypeName)

			mocksFnsCode += mockFnRetCallCode
			mocksFnsCode += mockFnRetMultiCallCode
		}

		if len(params) == 0 {
			continue
		}

		mockFnCallArgsCode, e := templates.MockFnCallArgs(methodTypeIdentifier, mockFnImportAlias)
		if e != nil {
			methodLog.Fatal("%v", e)
		}
		methodLog.Debug("%s code generated", templates.MockFnCallArgsTypeName)

		mockFnMultiCallArgsCode, e := templates.MockFnMultiCallArgs(methodTypeIdentifier, mockFnImportAlias)
		if e != nil {
			methodLog.Fatal("%v", e)
		}
		methodLog.Debug("%s code generated", templates.MockFnMultiCallArgsTypeName)

		mocksFnsCode += mockFnCallArgsCode + "\n"
		mocksFnsCode += mockFnMultiCallArgsCode + "\n"
		if len(results) != 0 {
			mockFnRetCallArgsCode, e := templates.MockFnRetCallArgs(methodTypeIdentifier, mockFnImportAlias, results)
			if e != nil {
				methodLog.Fatal("%v", e)
			}
			methodLog.Debug("%s code generated", templates.MockFnRetCallArgsTypeName)

			mockFnRetMultiCallArgsCode, e := templates.MockFnRetMultiCallArgs(methodTypeIdentifier, mockFnImportAlias, results)
			if e != nil {
				methodLog.Fatal("%v", e)
			}
			methodLog.Debug("%s code generated", templates.MockFnRetMultiCallArgsTypeName)

			mocksFnsCode += mockFnRetCallArgsCode
			mocksFnsCode += mockFnRetMultiCallArgsCode
		}
	}

	mockCode, e := templates.Mock(iTypeName.Name(), mockFnImportAlias, methods)
	if e != nil {
		log.Fatal("%v", e)
	}
	log.Debug("%s code generated...", templates.MockTypeName)

	return generatedCode{
		mockCode:        mockCode,
		mocksFnsCode:    mocksFnsCode,
		comparatorsCode: comparatorsCode,
	}
}

type generatedCode struct {
	mockCode        string
	mocksFnsCode    string
	comparatorsCode string
}
