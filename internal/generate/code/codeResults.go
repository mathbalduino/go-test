package code

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile/goImports"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-test/internal/generate/code/templates"
	"go/types"
)

func codeResults(signature *types.Signature, fileImports *goImports.GoImports, builtTypes map[string]bool, log *logCLI.LogCLI) []templates.Tuple {
	results := signature.Results()
	if results.Len() == 0 {
		return nil
	}

	tupleResults := make([]templates.Tuple, results.Len())
	for i := 0; i < results.Len(); i++ {
		currResult := results.At(i)
		resultLog := log.Debug("Analysing return value index %d...", i)
		resultTypeIdentifier := resolveTypeIdentifier(currResult, fileImports, resultLog)

		result := templates.Tuple{currResult.Name(), resultTypeIdentifier}
		if result.Name == "" {
			result.Name = fmt.Sprintf("r%d", i)
		}
		tupleResults[i] = result
	}

	return tupleResults
}
