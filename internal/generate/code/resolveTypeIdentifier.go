package code

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile/goImports"
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser/helpers"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"go/types"
)

func resolveTypeIdentifier(obj types.Object, i *goImports.GoImports, parentLog *logCLI.LogCLI) string {
	log := parentLog.Debug("Resolving type identifier...")
	callback := func(typename *types.Named) {
		obj := typename.Obj()
		if obj.Pkg() != nil && i.NeedImport(obj.Pkg().Path()) {
			i.AddImport(obj.Pkg().Name(), obj.Pkg().Path())
		}
	}
	helpers.CallbackOnNamedType(obj.Type(), callback, log)
	return helpers.ResolveTypeIdentifier(obj.Type(), i, log)
}
