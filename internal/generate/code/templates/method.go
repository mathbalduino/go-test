package templates

import "strings"

type Method struct {
	Name           string
	TypeIdentifier string
	Params         []Tuple
	Results        []Tuple
}

type Tuple struct {
	Name           string
	TypeIdentifier string
}

type comparator struct {
	Tuple
	ElementTypeIdentifier string
}

func paramToComparator(params []Tuple) []comparator {
	comparators := make([]comparator, len(params))
	for i, currParam := range params {
		typeIdentifier := currParam.TypeIdentifier
		if strings.HasPrefix(typeIdentifier, "...") {
			typeIdentifier = "[]" + typeIdentifier[3:]
		}

		comparators[i] = comparator{
			Tuple{
				currParam.Name,
				comparatorTypeName(typeIdentifier),
			},
			typeIdentifier,
		}
	}

	return comparators
}
