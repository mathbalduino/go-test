package templates

import (
	"fmt"
)

func Comparator(typeIdentifier string) string {
	return fmt.Sprintf(tmplComparator, comparatorTypeName(typeIdentifier), typeIdentifier)
}

const tmplComparator = `type %s = func(expected, received %s) bool`
