package templates

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"
)

var mock = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMock)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func Mock(interfaceTypeIdentifier, mockFnImportAlias string, methods []Method) (string, error) {
	type normalizedParam struct {
		Tuple
		UsageName string
	}
	type normalizedMethod struct {
		Method
		Params               []normalizedParam
		CustomMockFnTypeName string
		ImplMockFnTypeName   string
		MockedFnTypeName     string
	}
	type tmplVars struct {
		SuiteTypeIdentifier             string
		CustomMockFnName          string
		CustomMockBuilderTypeName string
		InterfaceTypeIdentifier   string
		ImplMockTypeName          string
		CustomMockedTypeName      string
		Methods                   []normalizedMethod
		CallMethodName            string
	}

	normalizedMethods := make([]normalizedMethod, len(methods))
	for i, currMethod := range methods {
		normalizedParams := make([]normalizedParam, len(currMethod.Params))
		for j, currParam := range currMethod.Params {
			p := normalizedParam{
				Tuple:     currParam,
				UsageName: currParam.Name,
			}
			if strings.HasPrefix(currParam.TypeIdentifier, "...") {
				p.UsageName += "..."
			}
			normalizedParams[j] = p
		}

		customMockFnTypeName := mockFnTypeName(currMethod.TypeIdentifier)
		normalizedMethods[i] = normalizedMethod{
			Method:               currMethod,
			Params:               normalizedParams,
			CustomMockFnTypeName: customMockFnTypeName,
			ImplMockFnTypeName:   privatizeTypeName(customMockFnTypeName),
			MockedFnTypeName:     mockedFnTypeName(currMethod.TypeIdentifier),
		}
	}

	customMockFnName := mockTypeName(interfaceTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnName,
		mockBuilderTypeName(interfaceTypeIdentifier),
		interfaceTypeIdentifier,
		privatizeTypeName(customMockFnName),
		mockedTypeName(interfaceTypeIdentifier),
		normalizedMethods,
		callMethodName,
	}

	var data bytes.Buffer
	e := mock.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMock = `
func {{.CustomMockFnName}}(suite {{.SuiteTypeIdentifier}}, callback func(mockMethod {{.CustomMockBuilderTypeName}}) {{.CustomMockedTypeName}}) {{.InterfaceTypeIdentifier}} {
	return &{{.ImplMockTypeName}}{
		suite,
		callback({{.CustomMockBuilderTypeName}}{
			{{range .Methods -}}
				{{.Name}}: &{{.ImplMockFnTypeName}}{ suite },
			{{end}}
		}),
	}
}

type {{.CustomMockBuilderTypeName}} struct {
{{range .Methods -}}
	{{.Name}} {{.CustomMockFnTypeName}}
{{end}}
}
type {{.CustomMockedTypeName}} struct {
{{range .Methods -}}
	{{.Name}} {{.MockedFnTypeName}}
{{end}}
}
type {{.ImplMockTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.CustomMockedTypeName}}
}

{{$implMockTypeName := .ImplMockTypeName}}
{{$callMethodName := .CallMethodName}}
{{$customMockedTypeName := .CustomMockedTypeName}}
{{range .Methods -}}
	func (__loxe_mock_impl {{$implMockTypeName}}) {{.Name}}({{range .Params}}{{.Name}} {{.TypeIdentifier}}, {{end}}) ({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {
		__loxe_mock_impl.suite.Helper()
		if __loxe_mock_impl.{{$customMockedTypeName}}.{{.Name}} == nil {
			__loxe_mock_impl.suite.Fatalf("The {{.Name}} method is not mocked, mock it in order to use it")
		}
		{{if .Results}}return{{end}} __loxe_mock_impl.{{$customMockedTypeName}}.{{.Name}}.{{$callMethodName}}({{range .Params}}{{.UsageName}}, {{end}})
	}
{{end}}
`
