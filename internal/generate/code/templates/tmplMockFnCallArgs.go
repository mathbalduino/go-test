package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnCallArgs = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMockFnCallArgs)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnCallArgs(functionTypeIdentifier, mockFnImportAlias string) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier string
		CustomMockFnCallArgsTypeName string
		ImplMockFnCallArgsTypeName   string
		CustomMockFnCallsTypeName    string
		ImplMockFnCallsTypeName      string
		MockedCallTypeIdentifier     string
		MockedCallsTypeIdentifier    string
		CustomMockedFnTypeName       string
		MockFnCallArgsTypeIdentifier string
		MockFnCallArgsTypeName       string

		BeforeMethodName    string
		BeforeAllMethodName string
		AfterMethodName     string
		AfterAllMethodName  string
		WaitingMethodName   string
		BindMethodName      string
		RepeatMethodName    string
	}

	customMockFnCallArgsTypeName := mockFnCallArgsTypeName(functionTypeIdentifier)
	customMockFnCallsTypeName := mockFnCallsTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnCallArgsTypeName,
		privatizeTypeName(customMockFnCallArgsTypeName),
		customMockFnCallsTypeName,
		privatizeTypeName(customMockFnCallsTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, mockedCallTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, mockedCallsTypeName),
		mockedFnTypeName(functionTypeIdentifier),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnCallArgsTypeName),
		MockFnCallArgsTypeName,

		beforeMethodName,
		beforeAllMethodName,
		afterMethodName,
		afterAllMethodName,
		waitingMethodName,
		bindMethodName,
		repeatMethodName,
	}

	var data bytes.Buffer
	e := mockFnCallArgs.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockFnCallArgs = `
type {{.CustomMockFnCallArgsTypeName}} interface {
	{{.BeforeMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallArgsTypeName}}
	{{.BeforeAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallArgsTypeName}}
	{{.AfterMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallArgsTypeName}}
	{{.AfterAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallArgsTypeName}}
	{{.WaitingMethodName}}(timeout time.Duration) {{.CustomMockFnCallArgsTypeName}}
	{{.BindMethodName}}(token *{{.MockedCallTypeIdentifier}}) {{.CustomMockedFnTypeName}}
	{{.RepeatMethodName}}(times uint) {{.CustomMockFnCallsTypeName}}
	{{.CustomMockedFnTypeName}}
}
type {{.ImplMockFnCallArgsTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnCallArgsTypeName}} {{.MockFnCallArgsTypeIdentifier}}
	{{.CustomMockedFnTypeName}}
}

func (__loxe_mockFn_call_args_impl *{{.ImplMockFnCallArgsTypeName}}) {{.BeforeMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallArgsTypeName}} {
	__loxe_mockFn_call_args_impl.suite.Helper()
	__loxe_mockFn_call_args_impl.{{.MockFnCallArgsTypeName}}.{{.BeforeMethodName}}(call, delay...)
	return __loxe_mockFn_call_args_impl
}
func (__loxe_mockFn_call_args_impl *{{.ImplMockFnCallArgsTypeName}}) {{.BeforeAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallArgsTypeName}} {
	__loxe_mockFn_call_args_impl.suite.Helper()
	__loxe_mockFn_call_args_impl.{{.MockFnCallArgsTypeName}}.{{.BeforeAllMethodName}}(calls, delay...)
	return __loxe_mockFn_call_args_impl
}
func (__loxe_mockFn_call_args_impl *{{.ImplMockFnCallArgsTypeName}}) {{.AfterMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallArgsTypeName}} {
	__loxe_mockFn_call_args_impl.suite.Helper()
	__loxe_mockFn_call_args_impl.{{.MockFnCallArgsTypeName}}.{{.AfterMethodName}}(call, delay...)
	return __loxe_mockFn_call_args_impl
}
func (__loxe_mockFn_call_args_impl *{{.ImplMockFnCallArgsTypeName}}) {{.AfterAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallArgsTypeName}} {
	__loxe_mockFn_call_args_impl.suite.Helper()
	__loxe_mockFn_call_args_impl.{{.MockFnCallArgsTypeName}}.{{.AfterAllMethodName}}(calls, delay...)
	return __loxe_mockFn_call_args_impl
}
func (__loxe_mockFn_call_args_impl *{{.ImplMockFnCallArgsTypeName}}) {{.WaitingMethodName}}(timeout time.Duration) {{.CustomMockFnCallArgsTypeName}} {
	__loxe_mockFn_call_args_impl.{{.MockFnCallArgsTypeName}}.{{.WaitingMethodName}}(timeout)
	return __loxe_mockFn_call_args_impl
}
func (__loxe_mockFn_call_args_impl *{{.ImplMockFnCallArgsTypeName}}) {{.BindMethodName}}(token *{{.MockedCallTypeIdentifier}}) {{.CustomMockedFnTypeName}} {
	__loxe_mockFn_call_args_impl.suite.Helper()
	__loxe_mockFn_call_args_impl.{{.MockFnCallArgsTypeName}}.{{.BindMethodName}}(token)
	return __loxe_mockFn_call_args_impl
}
func (__loxe_mockFn_call_args_impl *{{.ImplMockFnCallArgsTypeName}}) {{.RepeatMethodName}}(times uint) {{.CustomMockFnCallsTypeName}} {
	__loxe_mockFn_call_args_impl.suite.Helper()
	return &{{.ImplMockFnCallsTypeName}}{
		__loxe_mockFn_call_args_impl.suite,
		__loxe_mockFn_call_args_impl.{{.MockFnCallArgsTypeName}}.{{.RepeatMethodName}}(times),
		__loxe_mockFn_call_args_impl,
	}
}
`
