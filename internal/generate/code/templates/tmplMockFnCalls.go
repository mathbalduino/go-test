package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnCalls = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMockFnCalls)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnCalls(functionTypeIdentifier, mockFnImportAlias string) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier string
		CustomMockFnCallsTypeName string
		ImplMockFnCallsTypeName   string
		CustomMockedFnTypeName    string
		MockFnCallsTypeIdentifier string
		MockFnCallsTypeName       string
		MockedCallsTypeIdentifier string

		BindMethodName string
	}

	customMockFnCallsTypeName := mockFnCallsTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnCallsTypeName,
		privatizeTypeName(customMockFnCallsTypeName),
		mockedFnTypeName(functionTypeIdentifier),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnCallsTypeName),
		MockFnCallsTypeName,
		fmt.Sprintf("%s.%s", mockFnImportAlias, mockedCallsTypeName),

		bindMethodName,
	}

	var data bytes.Buffer
	e := mockFnCalls.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockFnCalls = `
type {{.CustomMockFnCallsTypeName}} interface {
	{{.BindMethodName}}(token *{{.MockedCallsTypeIdentifier}}) {{.CustomMockedFnTypeName}}
	{{.CustomMockedFnTypeName}}
}
type {{.ImplMockFnCallsTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnCallsTypeName}} {{.MockFnCallsTypeIdentifier}}
	{{.CustomMockedFnTypeName}}
}

func (__loxe_mockFn_calls_impl *{{.ImplMockFnCallsTypeName}}) {{.BindMethodName}}(token *{{.MockedCallsTypeIdentifier}}) {{.CustomMockedFnTypeName}} {
	__loxe_mockFn_calls_impl.suite.Helper()
	__loxe_mockFn_calls_impl.{{.MockFnCallsTypeName}}.{{.BindMethodName}}(token)
	return __loxe_mockFn_calls_impl
}
`
