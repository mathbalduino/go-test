package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnMulti = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMockFnMulti)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnMulti(functionTypeIdentifier, mockFnImportAlias string, params, results []Tuple) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier              string
		CustomMockFnMultiTypeName            string
		ImplMockFnMultiTypeName              string
		CustomMockFnRetMultiCallArgsTypeName string
		ImplMockFnRetMultiCallArgsTypeName   string
		CustomMockFnMultiCallArgsTypeName    string
		ImplMockFnMultiCallArgsTypeName      string
		CustomMockFnRetMultiCallTypeName     string
		ImplMockFnRetMultiCallTypeName       string
		CustomMockFnMultiCallTypeName        string
		ImplMockFnMultiCallTypeName          string
		MockFnMultiTypeIdentifier            string
		MockFnMultiTypeName                  string
		MockFnMultiStubTypeIdentifier        string
		Params                               []Tuple
		Results                              []Tuple

		IgnoringArgsMethodName string
		ExpectingMethodName    string
		AsStubMethodName       string
	}

	customMockFnMultiTypeName := mockFnMultiTypeName(functionTypeIdentifier)
	customMockFnRetMultiCallArgsTypeName := mockFnRetMultiCallArgsTypeName(functionTypeIdentifier)
	customMockFnMultiCallArgsTypeName := mockFnMultiCallArgsTypeName(functionTypeIdentifier)
	customMockFnRetMultiCallTypeName := mockFnRetMultiCallTypeName(functionTypeIdentifier)
	customMockFnMultiCallTypeName := mockFnMultiCallTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnMultiTypeName,
		privatizeTypeName(customMockFnMultiTypeName),
		customMockFnRetMultiCallArgsTypeName,
		privatizeTypeName(customMockFnRetMultiCallArgsTypeName),
		customMockFnMultiCallArgsTypeName,
		privatizeTypeName(customMockFnMultiCallArgsTypeName),
		customMockFnRetMultiCallTypeName,
		privatizeTypeName(customMockFnRetMultiCallTypeName),
		customMockFnMultiCallTypeName,
		privatizeTypeName(customMockFnMultiCallTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnMultiTypeName),
		MockFnMultiTypeName,
		fmt.Sprintf("%s.%s", mockFnImportAlias, mockFnMultiStubTypeName),
		params,
		results,

		ignoringArgsMethodName,
		expectingMethodName,
		asStubMethodName,
	}

	var data bytes.Buffer
	e := mockFnMulti.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockFnMulti = `
type {{.CustomMockFnMultiTypeName}} interface {
	{{if .Params -}}
		{{.IgnoringArgsMethodName}}() {{if .Results}} {{.CustomMockFnRetMultiCallArgsTypeName}} {{else}} {{.CustomMockFnMultiCallArgsTypeName}} {{end}}
	{{end -}}
	{{.ExpectingMethodName}}({{range .Params}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{if .Results}} {{.CustomMockFnRetMultiCallTypeName}} {{else}} {{.CustomMockFnMultiCallTypeName}} {{end}}
	{{.AsStubMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.MockFnMultiStubTypeIdentifier}}
}
type {{.ImplMockFnMultiTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnMultiTypeName}} {{.MockFnMultiTypeIdentifier}}
}

{{if .Params -}}
	func (__loxe_mockFn_multi_impl *{{.ImplMockFnMultiTypeName}}) {{.IgnoringArgsMethodName}}() {{if .Results}} {{.CustomMockFnRetMultiCallArgsTypeName}} {{else}} {{.CustomMockFnMultiCallArgsTypeName}} {{end}} {
		{{if .Results -}}
			return &{{.ImplMockFnRetMultiCallArgsTypeName}}{ __loxe_mockFn_multi_impl.suite, __loxe_mockFn_multi_impl.{{.MockFnMultiTypeName}}.{{.IgnoringArgsMethodName}}() }
		{{else -}}
			return &{{.ImplMockFnMultiCallArgsTypeName}}{ __loxe_mockFn_multi_impl.suite, __loxe_mockFn_multi_impl.{{.MockFnMultiTypeName}}.{{.IgnoringArgsMethodName}}() }
		{{end -}}
	}
{{end -}}
func (__loxe_mockFn_multi_impl *{{.ImplMockFnMultiTypeName}}) {{.ExpectingMethodName}}({{range .Params}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{if .Results}} {{.CustomMockFnRetMultiCallTypeName}} {{else}} {{.CustomMockFnMultiCallTypeName}} {{end}} {
	{{if .Results -}}
		return &{{.ImplMockFnRetMultiCallTypeName}}{ __loxe_mockFn_multi_impl.suite, __loxe_mockFn_multi_impl.{{.MockFnMultiTypeName}}.{{.ExpectingMethodName}}({{range .Params}}{{.Name}}, {{end}}) }
	{{else -}}
		return &{{.ImplMockFnMultiCallTypeName}}{ __loxe_mockFn_multi_impl.suite, __loxe_mockFn_multi_impl.{{.MockFnMultiTypeName}}.{{.ExpectingMethodName}}({{range .Params}}{{.Name}}, {{end}}) }
	{{end -}}
}
func (__loxe_mockFn_multi_impl *{{.ImplMockFnMultiTypeName}}) {{.AsStubMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.MockFnMultiStubTypeIdentifier}} {
	return __loxe_mockFn_multi_impl.{{.MockFnMultiTypeName}}.{{.AsStubMethodName}}({{range .Results}}{{.Name}}, {{end}})
}
`
