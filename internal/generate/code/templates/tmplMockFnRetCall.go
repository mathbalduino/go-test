package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnRetCall = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMockFnRetCall)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnRetCall(functionTypeIdentifier, mockFnImportAlias string, results []Tuple) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier string
		CustomMockFnRetCallTypeName string
		ImplMockFnRetCallTypeName   string
		CustomMockFnCallTypeName    string
		ImplMockFnCallTypeName      string
		ImplMockedFnTypeName        string
		MockFnCallTypeIdentifier    string
		MockFnCallTypeName          string
		Results                     []Tuple

		ReturningMethodName string
	}

	customMockFnRetCallTypeName := mockFnRetCallTypeName(functionTypeIdentifier)
	customMockFnCallTypeName := mockFnCallTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnRetCallTypeName,
		privatizeTypeName(customMockFnRetCallTypeName),
		customMockFnCallTypeName,
		privatizeTypeName(customMockFnCallTypeName),
		privatizeTypeName(mockedFnTypeName(functionTypeIdentifier)),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnCallTypeName),
		MockFnCallTypeName,
		results,

		returningMethodName,
	}

	var data bytes.Buffer
	e := mockFnRetCall.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockFnRetCall = `
type {{.CustomMockFnRetCallTypeName}} interface {
	{{.ReturningMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnCallTypeName}}
}
type {{.ImplMockFnRetCallTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnCallTypeName}} {{.MockFnCallTypeIdentifier}}
}

func (__loxe_mockFn_returning_impl *{{.ImplMockFnRetCallTypeName}}) {{.ReturningMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnCallTypeName}} {
	__loxe_mockFn_returning_impl.suite.Helper()
	__loxe_mockFn_returning_impl.{{.MockFnCallTypeName}}.{{.ReturningMethodName}}({{range .Results}}{{.Name}}, {{end}})
	return &{{.ImplMockFnCallTypeName}}{
		__loxe_mockFn_returning_impl.suite,
		__loxe_mockFn_returning_impl.{{.MockFnCallTypeName}}, 
		&{{.ImplMockedFnTypeName}}{ __loxe_mockFn_returning_impl.suite, __loxe_mockFn_returning_impl.{{.MockFnCallTypeName}} },
	}
}
`
