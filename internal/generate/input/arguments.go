package input

//go:generate go run gitlab.com/loxe-tools/go-test/internal/generate/input/generate --debug --source-type Arguments --source-package gitlab.com/loxe-tools/go-test/internal/generate/input

type Arguments struct {
	SourceFile         string
	SourceType         string
	Debug              bool
	SupportsANSI       bool
	OnlyAsciiTypenames bool
}
