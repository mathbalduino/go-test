package input

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
)

func PrintArguments(args Arguments, log *logCLI.LogCLI) {
	log.Debug(fmt.Sprintf(argsTmpl,
		MetaᐸArgumentsᐳ.SourceFile, args.SourceFile,
		MetaᐸArgumentsᐳ.SourceType, args.SourceType,

		MetaᐸArgumentsᐳ.Debug, args.Debug,
		MetaᐸArgumentsᐳ.SupportsANSI, args.SupportsANSI,
		MetaᐸArgumentsᐳ.OnlyAsciiTypenames, args.OnlyAsciiTypenames,
	))
}

const argsTmpl = `CLI arguments:
Source arguments:
	--%s: %s
	--%s: %s
Other arguments:
	--%s: %t
	--%s: %t
	--%s: %t`
