package mockCall

import "time"

func (m *MockCall) IgnoringArgs() {
	m.ignoreArgs = true
}
func (m *MockCall) Expecting(expectedArgs ...interface{}) {
	m.arguments = expectedArgs
}
func (m *MockCall) AsStub(stubResults ...interface{}) {
	m.stub = true
	m.results = stubResults
}
func (m *MockCall) Returning(functionResults ...interface{}) {
	m.results = functionResults
}
func (m *MockCall) UsingComparators(customComparators ...func(expected, received interface{}) bool) {
	m.comparators = customComparators
}
func (m *MockCall) Before(call MockedCall, offsetInterval ...time.Duration) {
	m.suite.Helper()
	call._after(m, offsetInterval...)

	// Since there's only the 'after' method implementation, when calls are copied
	// is necessary to signal to all the other calls that this new copied mock needs
	// to be added as a 'previous' call too
	m.nextCalls = append(m.nextCalls, func(mockedCall MockedCall) {
		call._after(mockedCall, offsetInterval...)
	})
}
func (m *MockCall) After(call MockedCall, offsetInterval ...time.Duration) {
	m.suite.Helper()
	m._after(call, offsetInterval...)
}
func (m *MockCall) Waiting(timeout time.Duration) {
	m.timeout = timeout
}
