package mockCall

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"time"
)

func New(suite suite, skipFrames ...uint) *MockCall {
	localSkipFrames := uint(1)
	if len(skipFrames) > 0 {
		localSkipFrames += skipFrames[0]
	}

	return &MockCall{
		// Don't expect these fields to change after the first definition
		suite,
		internal.FnCallFileLine(localSkipFrames),
		false,
		false,
		0,
		nil,
		nil,
		nil,
		nil,

		&[]*orderedCall{},
		make(chan byte, 1),
		nil,
	}
}

type MockCall struct {
	// Immutable during test execution fields (Cannot be changed after declared)
	suite         suite
	fileLine      string
	stub          bool
	ignoreArgs    bool
	timeout       time.Duration
	arguments     []interface{}
	results       []interface{}
	comparators   []func(a, b interface{}) bool
	nextCalls     []func(MockedCall) // Used only when copying a *MockCall

	// Mutable during test execution (can be changed after declaration)
	previousCalls *[]*orderedCall    // Pointer to slice to guarantee that all the copied *MockCalls share the same slice
	channel chan byte
	call    *call
}
type suite = interface {
	Helper()
	Fatalf(string, ...interface{})
}
