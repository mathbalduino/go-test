package mockCall

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"testing"
	"time"
)

func TestMockedCall(t *testing.T) {
	suite := &internal.TestSuite{t}

	suite.Run("_after", func(suite *internal.TestSuite) {
		suite.Run("Should call the suite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {
				helperCalls += 1
			}}
			mock := New(mockSuite)

			mock._after(nil)
			if helperCalls != 2 { // consider that the 'newOrderedCall' method calls the 'Helper' method too
				suite.Fatalf("The '_after' method didn't called the suite 'Helper' method correctly")
			}
		})
		suite.Run("When there's no other previous call, just set the 'previousCalls' field to a singleton with the given call", func(suite *internal.TestSuite) {
			mock := New(suite)
			call := New(suite)

			mock._after(call)
			if len(*mock.previousCalls) != 1 || (*mock.previousCalls)[0].MockedCall.(*MockCall) != call {
				suite.Fatalf("The method '_after' didn't set the 'previousCalls' field properly")
			}
		})
		suite.Run("When there's other previous calls, should just append the new call to the end of the 'previousCalls' field, preserving the old ones", func(suite *internal.TestSuite) {
			mock := New(suite)
			call := New(suite)
			old := New(suite)
			mock._after(old)

			mock._after(call)
			if len(*mock.previousCalls) != 2 {
				suite.Fatalf("The method '_after' didn't set the 'previousCalls' field properly")
			}
			if (*mock.previousCalls)[0].MockedCall.(*MockCall) != old {
				suite.Fatalf("The method '_after' didn't set the 'previousCalls' field properly")
			}
			if (*mock.previousCalls)[1].MockedCall.(*MockCall) != call {
				suite.Fatalf("The method '_after' didn't set the 'previousCalls' field properly")
			}
		})
		suite.Run("Should set half the offset interval, if set (one variadic argument)", func(suite *internal.TestSuite) {
			mock := New(suite)
			call := New(suite)
			minOffset := time.Millisecond

			mock._after(call, minOffset)
			if (*mock.previousCalls)[0].minOffset != minOffset {
				suite.Fatalf("The method '_after' didn't set the 'minOffset' fields of the previous call properly")
			}
		})
		suite.Run("Should set full offset interval, if set (two variadic arguments)", func(suite *internal.TestSuite) {
			mock := New(suite)
			call := New(suite)
			minOffset, maxOffset := time.Millisecond, time.Second

			mock._after(call, minOffset, maxOffset)
			if (*mock.previousCalls)[0].minOffset != minOffset || (*mock.previousCalls)[0].maxOffset != maxOffset {
				suite.Fatalf("The method '_after' didn't set the offset interval of the previous call properly")
			}
		})
	})
	suite.Run("Method 'FileLine' should return the mock file and line as a string", func(suite *internal.TestSuite) {
		mock := New(suite)
		mock.fileLine = "Directly editing the field just to easy the implementation"

		if mock.fileLine != mock.FileLine() {
			suite.Fatalf("The 'FileLine' method return the wrong file and line string")
		}
	})
	suite.Run("Method '_getCall' returns the mock call", func(suite *internal.TestSuite) {
		mock := New(suite)
		mock.call = &call{}

		if mock.call != mock._getCall() {
			suite.Fatalf("The '_getCall' method return the wrong call struct pointer")
		}
	})
}
