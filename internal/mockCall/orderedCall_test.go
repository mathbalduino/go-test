package mockCall

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"testing"
	"time"
)

func TestOrderedCall(t *testing.T) {
	suite := &internal.TestSuite{t}

	// There's no need to exercise scenarios with the 'Before' method, because the 'Before' method
	// is implemented using the 'After' method itself
	suite.Run("checkPreviousCalls", func(suite *internal.TestSuite) {
		suite.Run("If the 'previousCalls' slice is empty, just return the function (don't throw failure)", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)

			mock.checkPreviousCalls(&call{})
			if fatalfCalls != 0 {
				suite.Fatalf("The 'checkPreviousCalls' threw failure with zero empty 'previousCalls' slice")
			}
		})
		suite.Run("If the given previous call didn't had been called, throw failure", func(suite *internal.TestSuite) {
			fileLine := "custom file line"
			prevFileLine := "custom fileLine 2"
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != previousCallNotCalledError {
						suite.Fatalf("The 'checkPreviousCalls' didn't threw the correct failure message when the given previous call didn't had been called")
					}
					if len(args) != 2 || args[0].(string) != fileLine || args[1].(string) != prevFileLine {
						suite.Fatalf("The 'checkPreviousCalls' didn't used the correct error message arguments when the given previous call didn't had been called")
					}
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)
			mock.fileLine = fileLine
			prev := New(suite)
			prev.fileLine = prevFileLine

			mock.After(prev)
			mock.checkPreviousCalls(&call{})
			if fatalfCalls != 1 {
				suite.Fatalf("The 'checkPreviousCalls' didn't threw failure when the given previous call didn't had been called")
			}
		})
		suite.Run("If the given previous call didn't happened BEFORE the mock, throw failure", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			fileLine := "custom fileLine"
			prevFileLine := "custom fileLine 2"
			callTimestamp := time.Now()
			prevCallTimestamp := callTimestamp.Add(time.Second)
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != previousCallHappenedAfterError {
						suite.Fatalf("The 'checkPreviousCalls' didn't threw the correct failure message when the given previous call happened AFTER the mock")
					}
					if len(args) != 3 ||
						args[0].(string) != fileLine ||
						args[1].(string) != prevFileLine ||
						args[2].(time.Duration) != prevCallTimestamp.Sub(callTimestamp) {
						suite.Fatalf("The 'checkPreviousCalls' didn't used the correct error message arguments when the given previous call happened AFTER the mock")
					}
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)
			mock.fileLine = fileLine
			mock.call = &call{timestamp: callTimestamp}
			prev := New(suite)
			prev.fileLine = prevFileLine
			prev.call = &call{timestamp: prevCallTimestamp}

			mock.After(prev)
			mock.checkPreviousCalls(mock.call)
			if fatalfCalls != 1 {
				suite.Fatalf("The 'checkPreviousCalls' didn't threw failure when the given previous call happened AFTER the mock")
			}
		})
		suite.Run("If there's no offset interval, and the given call happened BEFORE the mock, don't throw failure", func(suite *internal.TestSuite) {
			now := time.Now()
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)
			mock.call = &call{timestamp: now}
			prev := New(suite)
			prev.call = &call{timestamp: now.Add(-time.Second)}

			mock.After(prev)
			mock.checkPreviousCalls(mock.call)
			if fatalfCalls != 0 {
				suite.Fatalf("The 'checkPreviousCalls' threw failure when the given previous call actually happened BEFORE the mock")
			}
		})
		suite.Run("If there's a minimum offset interval and the given call happened AFTER this minimum offset, throw failure", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			fileLine := "custom fileLine"
			prevFileLine := "custom fileLine 2"
			minOffset := time.Second * 2
			callTimestamp := time.Now()
			prevCallTimestamp := callTimestamp.Add(-time.Second)
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != previousCallHappenedAfterMinOffsetError {
						suite.Fatalf("The 'checkPreviousCalls' didn't threw the correct failure message when the given previous call happened AFTER the mock (with minimum offset)")
					}
					if len(args) != 4 ||
						args[0].(string) != fileLine ||
						args[1].(time.Duration) != minOffset ||
						args[2].(string) != prevFileLine ||
						args[3].(time.Duration) != callTimestamp.Sub(prevCallTimestamp) {
						suite.Fatalf("The 'checkPreviousCalls' didn't used the correct error message arguments when the given previous call happened AFTER the mock (with minimum offset)")
					}
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)
			mock.fileLine = fileLine
			mock.call = &call{timestamp: callTimestamp}
			prev := New(suite)
			prev.fileLine = prevFileLine
			prev.call = &call{timestamp: prevCallTimestamp}

			mock.After(prev, minOffset)
			mock.checkPreviousCalls(mock.call)
			if fatalfCalls != 1 {
				suite.Fatalf("The 'checkPreviousCalls' didn't threw failure when the given previous call happened AFTER the mock (with minimum offset)")
			}
		})
		suite.Run("If the given call happened BEFORE the mock (plus the minimum offset), don't throw failure", func(suite *internal.TestSuite) {
			now := time.Now()
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)
			mock.call = &call{timestamp: now}
			prev := New(suite)
			prev.call = &call{timestamp: now.Add(-time.Second * 2)}

			mock.After(prev, time.Second)
			mock.checkPreviousCalls(mock.call)
			if fatalfCalls != 0 {
				suite.Fatalf("The 'checkPreviousCalls' threw failure when the given previous call actually happened BEFORE the mock (respecting the minimum offset)")
			}
		})
		suite.Run("With full offset interval, if given call happens BEFORE the maximum offset, throw failure", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			fileLine := "custom fileLine"
			prevFileLine := "custom fileLine 2"
			maxOffset := time.Second * 2
			callTimestamp := time.Now()
			prevCallTimestamp := callTimestamp.Add(-time.Second * 3)
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != previousCallHappenedBeforeMaxOffsetError {
						suite.Fatalf("The 'checkPreviousCalls' didn't threw the correct failure message when the given previous call happened BEFORE the mock (plus maximum offset)")
					}
					if len(args) != 4 ||
						args[0].(string) != fileLine ||
						args[1].(time.Duration) != maxOffset ||
						args[2].(string) != prevFileLine ||
						args[3].(time.Duration) != callTimestamp.Sub(prevCallTimestamp) {
						suite.Fatalf("The 'checkPreviousCalls' didn't used the correct error message arguments when the given previous call happened BEFORE the mock (plus maximum offset)")
					}
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)
			mock.fileLine = fileLine
			mock.call = &call{timestamp: callTimestamp}
			prev := New(suite)
			prev.fileLine = prevFileLine
			prev.call = &call{timestamp: prevCallTimestamp}

			mock.After(prev, time.Second, maxOffset)
			mock.checkPreviousCalls(mock.call)
			if fatalfCalls != 1 {
				suite.Fatalf("The 'checkPreviousCalls' didn't threw failure when the given previous call happened BEFORE the mock (plus maximum offset)")
			}
		})
		suite.Run("If the given call happened inside the specified offset interval, don't throw failure", func(suite *internal.TestSuite) {
			now := time.Now()
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)
			mock.call = &call{timestamp: now}
			prev := New(suite)
			prev.call = &call{timestamp: now.Add(-time.Second * 2)}

			mock.After(prev, time.Second, time.Second*3)
			mock.checkPreviousCalls(mock.call)
			if fatalfCalls != 0 {
				suite.Fatalf("The 'checkPreviousCalls' threw failure when the given previous call actually happened respecting the offset interval")
			}
		})
	})
	suite.Run("newOrderedCall", func(suite *internal.TestSuite) {
		suite.Run("Should call the suite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {
				helperCalls += 1
			}}
			mock := New(mockSuite)

			mock.newOrderedCall(nil)
			if helperCalls != 1 {
				suite.Fatalf("The 'newOrderedCall' method didn't called the suite 'Helper' method correctly")
			}
		})
		suite.Run("Without offset interval, use default values", func(suite *internal.TestSuite) {
			mock := New(suite)
			otherMock := New(suite)

			orderedCall := mock.newOrderedCall(otherMock)
			if orderedCall == nil ||
				orderedCall.MockedCall.(*MockCall) != otherMock ||
				orderedCall.minOffset != 0 ||
				orderedCall.maxOffset != -1 {
				suite.Fatalf("The 'newOrderedCall' didn't created a correct orderedCall (without offset interval)")
			}
		})
		suite.Run("With minimum offset interval, use default value to maximum offset", func(suite *internal.TestSuite) {
			mock := New(suite)
			otherMock := New(suite)
			minOffset := time.Second

			orderedCall := mock.newOrderedCall(otherMock, minOffset)
			if orderedCall == nil ||
				orderedCall.MockedCall.(*MockCall) != otherMock ||
				orderedCall.minOffset != minOffset ||
				orderedCall.maxOffset != -1 {
				suite.Fatalf("The 'newOrderedCall' didn't created a correct orderedCall (with minimum offset)")
			}
		})
		suite.Run("With full offset interval, don't use default values", func(suite *internal.TestSuite) {
			mock := New(suite)
			otherMock := New(suite)
			minOffset := time.Second
			maxOffset := time.Second * 3

			orderedCall := mock.newOrderedCall(otherMock, minOffset, maxOffset)
			if orderedCall == nil ||
				orderedCall.MockedCall.(*MockCall) != otherMock ||
				orderedCall.minOffset != minOffset ||
				orderedCall.maxOffset != maxOffset {
				suite.Fatalf("The 'newOrderedCall' didn't created a correct orderedCall (with full offset)")
			}
		})
		suite.Run("Throw failure when the offset interval values are equal", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != offsetIntervalValuesOverlapError {
						suite.Fatalf("The 'newOrderedCall' method didn't throw the correct failure error message when offset values are equal")
					}
					if len(args) != 0 {
						suite.Fatalf("The 'newOrderedCall' method gave arguments to the failure error message when offset values are equal")
					}
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)
			otherMock := New(suite)
			minOffset := time.Second
			maxOffset := time.Second

			mock.newOrderedCall(otherMock, minOffset, maxOffset)
			if fatalfCalls != 1 {
				suite.Fatalf("The 'newOrderedCall' method didn't throw failure error with offset values that are equal")
			}
		})
		suite.Run("Throw failure when the offset interval values overlap", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != offsetIntervalValuesOverlapError {
						suite.Fatalf("The 'newOrderedCall' method didn't throw the correct failure error message when overlapping offset values")
					}
					if len(args) != 0 {
						suite.Fatalf("The 'newOrderedCall' method gave arguments to the failure error message when overlapping offset values")
					}
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)
			otherMock := New(suite)
			minOffset := time.Second * 3
			maxOffset := time.Second

			mock.newOrderedCall(otherMock, minOffset, maxOffset)
			if fatalfCalls != 1 {
				suite.Fatalf("The 'newOrderedCall' method didn't throw failure error with overlapping offset values")
			}
		})
	})
}
