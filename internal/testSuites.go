package internal

import (
	"gitlab.com/loxe-tools/go-base-library/util"
	"testing"
)

type TestSuite struct {
	*testing.T
}

func (t *TestSuite) Run(name string, fn func(suite *TestSuite)) {
	t.T.Run(name, func(t *testing.T) {
		fn(&TestSuite{t})
	})
}

func (t *TestSuite) Fatalf(msg string, args ...interface{}) {
	t.T.Helper()
	t.T.Fatalf("\n"+util.BoldRedString(msg), args...)
}

// -----

type MockSuite struct {
	Fatalf_  func(msg string, args ...interface{})
	Cleanup_ func(f func())
	Helper_  func()
}

func (s *MockSuite) Fatalf(msg string, args ...interface{}) { s.Fatalf_(msg, args...) }
func (s *MockSuite) Cleanup(f func())                       { s.Cleanup_(f) }
func (s *MockSuite) Helper()                                { s.Helper_() }
