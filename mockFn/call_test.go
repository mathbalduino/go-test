package mockFn

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"reflect"
	"testing"
	"time"
)

func TestMockFn_Call(t *testing.T) {
	suite := &internal.TestSuite{t}

	suite.Run("Should throw failure if the current call is unexpected", func(suite *internal.TestSuite) {
		fatalfCalls := 0
		fileLine := "custom fileLine"
		args := []interface{}{1, "value"}
		mockSuite := &internal.MockSuite{
			Helper_: func() {},
			Fatalf_: func(msg string, receivedArgs ...interface{}) {
				fatalfCalls += 1
				if msg != unexpectedCallError {
					suite.Fatalf("The 'Call' method threw the wrong failure message when received an unexpected call")
				}
				if len(receivedArgs) != 2 ||
					receivedArgs[0].(string) != displayArgs(args...) ||
					receivedArgs[1].(string) != fileLine {
					suite.Fatalf("The 'Call' method gave the wrong arguments to the failure message when received an unexpected call")
				}
			},
		}
		mock := &mockFn{suite: mockSuite, fileLine: fileLine, syncCalls: nil, asyncCalls: nil}

		mock.Call(args...)
		if fatalfCalls != 1 {
			suite.Fatalf("The 'Call' method didn't correctly threw a failure when receives an unexpected call")
		}
	})
	suite.Run("Should call MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Helper_: func() { helperCalls += 1 },

			// On purpose cause an "unexpected call" failure, to shorten the test
			Fatalf_: func(msg string, receivedArgs ...interface{}) {},
		}
		mock := &mockFn{suite: mockSuite, syncCalls: nil, asyncCalls: nil}

		mock.Call()
		if helperCalls != 1 {
			suite.Fatalf("The 'Call' method didn't correctly called the MockSuite 'Helper' method")
		}
	})
	suite.Run("The mutex 'Lock', the 'matchArgs', 'call' and the mutex 'Unlock' methods calls must happen sequentially, when next call is Sync", func(suite *internal.TestSuite) {
		mockMutex := &mockMutex{}
		var matchArgsCallTimestamp, callCallTimestamp time.Time
		mock := &mockFn{
			mutex: mockMutex,
			suite: &internal.MockSuite{
				Helper_: func() {},
				Fatalf_: func(msg string, receivedArgs ...interface{}) {},
			},
			syncCalls: []MockedCall{
				&mockMockedCall{
					matchArgs_: func(...interface{}) bool {
						matchArgsCallTimestamp = time.Now()
						return true
					},
					call_: func(i ...interface{}) []interface{} {
						callCallTimestamp = time.Now()
						return nil
					},
				},
			},
		}

		mock.Call()
		if len(mockMutex.lockCalls) != 1 || len(mockMutex.unlockCalls) != 1 {
			suite.Fatalf("The 'Call' method didn't correctly handled the mutex lock/unlock for async calls")
		}
		if callCallTimestamp.After(mockMutex.unlockCalls[0]) {
			suite.Fatalf("The 'Call' method called the 'call' method after the 'Unlock' mutex method, for async calls")
		}
		if matchArgsCallTimestamp.After(callCallTimestamp) {
			suite.Fatalf("The 'Call' method called the 'matchArgs' method after the 'call' method, for async calls")
		}
		if mockMutex.lockCalls[0].After(matchArgsCallTimestamp) {
			suite.Fatalf("The 'Call' method called the 'Lock' mutex method after the 'called' method, for async calls")
		}
	})
	suite.Run("If the next call is sync and the received arguments don't match, throw failure", func(suite *internal.TestSuite) {
		fatalfCalls := 0
		fileLine := "custom fileLine"
		expectedArgs := []interface{}{1, "value"}
		args := []interface{}{1, "value", "extra element"}
		mockSuite := &internal.MockSuite{
			Helper_: func() {},
			Fatalf_: func(msg string, receivedArgs ...interface{}) {
				fatalfCalls += 1
				if msg != wrongNextOrderedCallError {
					suite.Fatalf("The 'Call' method threw the wrong failure message when the sync call doesn't matches")
				}
				if len(receivedArgs) != 3 ||
					receivedArgs[0].(string) != displayArgs(args...) ||
					receivedArgs[1].(string) != displayArgs(expectedArgs...) ||
					receivedArgs[2].(string) != fileLine {
					suite.Fatalf("The 'Call' method gave the wrong arguments to the failure message when the sync call doesn't matches")
				}
			},
		}
		mock := &mockFn{suite: mockSuite, fileLine: fileLine, mutex: &mockMutex{}, syncCalls: []MockedCall{&mockMockedCall{
			matchArgs_:    func(i ...interface{}) bool { return false }, // important to return false
			expectedArgs_: func() []interface{} { return expectedArgs },
		}}}

		mock.Call(args...)
		if fatalfCalls != 1 {
			suite.Fatalf("The 'Call' method didn't threw a failure when the sync call doesn't matches")
		}
	})
	suite.Run("When next call is sync, ensure that the received arguments are forwarded to the 'matchArgs' method", func(suite *internal.TestSuite) {
		args := []interface{}{1, "value"}
		mock := &mockFn{
			suite: &internal.MockSuite{
				Helper_: func() {},
				Fatalf_: func(msg string, args ...interface{}) {},
			},
			mutex: &mockMutex{},
			syncCalls: []MockedCall{&mockMockedCall{
				matchArgs_: func(receivedArgs ...interface{}) bool {
					if !reflect.DeepEqual(receivedArgs, args) {
						suite.Fatalf("The 'Call' method didn't correctly forwarded received arguments to the 'matchArgs' method")
					}
					return true
				},
				call_: func(i ...interface{}) []interface{} { return nil },
			}},
		}

		mock.Call(args...)
	})
	suite.Run("When next call is sync, ensure that the received arguments are forwarded to the 'call' method", func(suite *internal.TestSuite) {
		args := []interface{}{1, "value"}
		mock := &mockFn{
			suite: &internal.MockSuite{
				Helper_: func() {},
				Fatalf_: func(msg string, args ...interface{}) {},
			},
			mutex: &mockMutex{},
			syncCalls: []MockedCall{&mockMockedCall{
				matchArgs_: func(receivedArgs ...interface{}) bool { return true },
				call_: func(receivedArgs ...interface{}) []interface{} {
					if !reflect.DeepEqual(receivedArgs, args) {
						suite.Fatalf("The 'Call' method didn't correctly forwarded received arguments to the 'call' method")
					}

					return nil
				},
			}},
		}

		mock.Call(args...)
	})
	suite.Run("If the next call is sync and the received arguments match, don't throw failure and return this call results", func(suite *internal.TestSuite) {
		fatalfCalls := 0
		results := []interface{}{1, "value"}
		mock := &mockFn{
			suite: &internal.MockSuite{
				Helper_: func() {},
				Fatalf_: func(msg string, args ...interface{}) { fatalfCalls += 1 },
			},
			mutex: &mockMutex{},
			syncCalls: []MockedCall{&mockMockedCall{
				matchArgs_: func(i ...interface{}) bool { return true }, // must return true
				call_:      func(i ...interface{}) []interface{} { return results },
			}},
		}

		receivedArgs := mock.Call()
		if fatalfCalls != 0 {
			suite.Fatalf("The 'Call' method called the MockSuite 'Fatalf' method when the sync call matched")
		}
		if !reflect.DeepEqual(receivedArgs, results) {
			suite.Fatalf("The 'Call' method didn't returned the correct mock results")
		}
	})
	suite.Run("The mutex 'Lock', the 'called', 'matchArgs', 'call' and the mutex 'Unlock' methods calls must happen sequentially, when next call is Async", func(suite *internal.TestSuite) {
		mockMutex := &mockMutex{}
		var calledCallTimestamp, matchArgsCallTimestamp, callCallTimestamp time.Time
		mock := &mockFn{
			mutex: mockMutex,
			suite: &internal.MockSuite{
				Helper_: func() {},
				Fatalf_: func(msg string, receivedArgs ...interface{}) {},
			},
			asyncCalls: []MockedCall{
				&mockMockedCall{
					called_: func() bool {
						calledCallTimestamp = time.Now()
						return false
					},
					matchArgs_: func(...interface{}) bool {
						matchArgsCallTimestamp = time.Now()
						return true
					},
					call_: func(i ...interface{}) []interface{} {
						callCallTimestamp = time.Now()
						return nil
					},
				},
			},
		}

		mock.Call()
		if len(mockMutex.lockCalls) != 1 || len(mockMutex.unlockCalls) != 1 {
			suite.Fatalf("The 'Call' method didn't correctly handled the mutex lock/unlock for async calls")
		}
		if callCallTimestamp.After(mockMutex.unlockCalls[0]) {
			suite.Fatalf("The 'Call' method called the 'call' method after the 'Unlock' mutex method, for async calls")
		}
		if matchArgsCallTimestamp.After(callCallTimestamp) {
			suite.Fatalf("The 'Call' method called the 'matchArgs' method after the 'call' method, for async calls")
		}
		if calledCallTimestamp.After(matchArgsCallTimestamp) {
			suite.Fatalf("The 'Call' method called the 'called' method after the 'matchArgs' method, for async calls")
		}
		if mockMutex.lockCalls[0].After(calledCallTimestamp) {
			suite.Fatalf("The 'Call' method called the 'Lock' mutex method after the 'called' method, for async calls")
		}
	})
	suite.Run("If the next call is async and the received arguments don't match none of the registered async calls, throw failure", func(suite *internal.TestSuite) {
		fatalfCalls := 0
		fileLine := "custom fileLine"
		args := []interface{}{1, "value"}
		mockSuite := &internal.MockSuite{
			Helper_: func() {},
			Fatalf_: func(msg string, receivedArgs ...interface{}) {
				fatalfCalls += 1
				if msg != unexpectedAsyncCallError {
					suite.Fatalf("The 'Call' method didn't used the correct failure message when there are not async calls that match the args")
				}
				if len(receivedArgs) != 2 ||
					receivedArgs[0].(string) != displayArgs(args...) ||
					receivedArgs[1].(string) != fileLine {
					suite.Fatalf("The 'Call' method gave the wrong arguments to the failure message when there are not async calls that match the args")
				}
			},
		}
		mock := &mockFn{
			mutex:    &mockMutex{},
			suite:    mockSuite,
			fileLine: fileLine,
			asyncCalls: []MockedCall{
				&mockMockedCall{called_: func() bool { return false }, matchArgs_: func(...interface{}) bool { return false }},
				&mockMockedCall{called_: func() bool { return true }, matchArgs_: func(...interface{}) bool { return false }},
				// If active, this case would match
				//&mockMockedCall{called_: func() bool { return false }, matchArgs_: func(...interface{}) bool { return true }},
				&mockMockedCall{called_: func() bool { return true }, matchArgs_: func(...interface{}) bool { return true }},
			},
		}

		mock.Call(args...)
		if fatalfCalls != 1 {
			suite.Fatalf("The 'Call' method didn't correctly threw a failure when there are not async calls that match the args")
		}
	})
	suite.Run("When next call is async, ensure that the received arguments are forwarded to the 'matchArgs' method", func(suite *internal.TestSuite) {
		args := []interface{}{1, "value"}
		mock := &mockFn{
			suite: &internal.MockSuite{
				Helper_: func() {},
				Fatalf_: func(msg string, args ...interface{}) {},
			},
			mutex: &mockMutex{},
			asyncCalls: []MockedCall{&mockMockedCall{
				matchArgs_: func(receivedArgs ...interface{}) bool {
					if !reflect.DeepEqual(receivedArgs, args) {
						suite.Fatalf("The 'Call' method didn't correctly forwarded received arguments to the 'matchArgs' method")
					}
					return true
				},
				called_: func() bool { return false },
				call_:   func(i ...interface{}) []interface{} { return nil },
			}},
		}

		mock.Call(args...)
	})
	suite.Run("When next call is async, ensure that the received arguments are forwarded to the 'call' method", func(suite *internal.TestSuite) {
		args := []interface{}{1, "value"}
		mock := &mockFn{
			suite: &internal.MockSuite{
				Helper_: func() {},
				Fatalf_: func(msg string, args ...interface{}) {},
			},
			mutex: &mockMutex{},
			asyncCalls: []MockedCall{&mockMockedCall{
				matchArgs_: func(receivedArgs ...interface{}) bool { return true },
				called_:    func() bool { return false },
				call_: func(receivedArgs ...interface{}) []interface{} {
					if !reflect.DeepEqual(receivedArgs, args) {
						suite.Fatalf("The 'Call' method didn't correctly forwarded received arguments to the 'call' method")
					}

					return nil
				},
			}},
		}

		mock.Call(args...)
	})
	suite.Run("If the next call is async, not already called and the received arguments match one of the registered async calls, don't throw failure and return this call results", func(suite *internal.TestSuite) {
		fatalfCalls := 0
		results := []interface{}{1, "value"}
		mock := &mockFn{
			mutex: &mockMutex{},
			suite: &internal.MockSuite{
				Helper_: func() {},
				Fatalf_: func(msg string, receivedArgs ...interface{}) {
					fatalfCalls += 1
				},
			},
			asyncCalls: []MockedCall{
				&mockMockedCall{called_: func() bool { return false }, matchArgs_: func(...interface{}) bool { return false }},
				&mockMockedCall{called_: func() bool { return true }, matchArgs_: func(...interface{}) bool { return false }},
				&mockMockedCall{
					called_:    func() bool { return false },
					matchArgs_: func(...interface{}) bool { return true },
					call_:      func(i ...interface{}) []interface{} { return results },
				},
				&mockMockedCall{called_: func() bool { return true }, matchArgs_: func(...interface{}) bool { return true }},
			},
		}

		receivedResults := mock.Call()
		if fatalfCalls != 0 {
			suite.Fatalf("The 'Call' method threw a failure even with the async call matching")
		}
		if !reflect.DeepEqual(receivedResults, results) {
			suite.Fatalf("The 'Call' method didn't returned the correct results")
		}
	})
}
