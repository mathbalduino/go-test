package mockFn

import (
	"gitlab.com/loxe-tools/go-test/internal/mockCall"
	"time"
)

// Interface used just to easy tests (allow mocks)
type iMockCall interface {
	// Builder methods
	IgnoringArgs()
	Expecting(expectedArgs ...interface{})
	AsStub(stubResults ...interface{})
	Returning(functionResults ...interface{})
	UsingComparators(customComparators ...func(expected, received interface{}) bool)
	Before(call mockCall.MockedCall, offsetInterval ...time.Duration)
	After(call mockCall.MockedCall, offsetInterval ...time.Duration)
	Waiting(timeout time.Duration)

	// Util methods
	ExpectedArgs() []interface{}
	Called() bool
	Wait()
	MatchArgs(receivedArgs ...interface{}) bool
	Call(receivedArgs ...interface{}) []interface{}
	Copy() *mockCall.MockCall
	mockCall.MockedCall
}
