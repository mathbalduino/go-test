package mockFn

type MockFnCalls interface {
	Bind(token *MockedCalls) MockedFn
	MockedFn
}
