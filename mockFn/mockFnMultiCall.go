package mockFn

import (
	"time"
)

type MockFnMultiCall interface {
	Returning(functionResults ...interface{}) MockFnMultiCall
	UsingComparators(customComparators ...func(expected, received interface{}) bool) MockFnMultiCall
	Before(call MockedCall, offsetInterval ...time.Duration) MockFnMultiCall
	BeforeAll(call MockedCalls, offsetInterval ...time.Duration) MockFnMultiCall
	After(call MockedCall, offsetInterval ...time.Duration) MockFnMultiCall
	AfterAll(call MockedCalls, offsetInterval ...time.Duration) MockFnMultiCall
	Waiting(timeout time.Duration) MockFnMultiCall
	Bind(token *MockedCall) MockedCalls
	Repeat(times uint) MockFnMultiCalls
	MockedCalls
}
type mockFnMultiCall struct {
	*mockFnCall
}

func (w *mockFnMultiCall) Returning(functionResults ...interface{}) MockFnMultiCall {
	w.mockFnCall.Returning(functionResults...)
	return w
}
func (w *mockFnMultiCall) UsingComparators(customComparators ...func(expected, received interface{}) bool) MockFnMultiCall {
	w.mockFnCall.UsingComparators(customComparators...)
	return w
}
func (w *mockFnMultiCall) Before(call MockedCall, offsetInterval ...time.Duration) MockFnMultiCall {
	w.suite.Helper()
	w.mockFnCall.Before(call, offsetInterval...)
	return w
}
func (w *mockFnMultiCall) BeforeAll(calls MockedCalls, offsetInterval ...time.Duration) MockFnMultiCall {
	w.suite.Helper()
	w.mockFnCall.BeforeAll(calls, offsetInterval...)
	return w
}
func (w *mockFnMultiCall) After(call MockedCall, offsetInterval ...time.Duration) MockFnMultiCall {
	w.suite.Helper()
	w.mockFnCall.After(call, offsetInterval...)
	return w
}
func (w *mockFnMultiCall) AfterAll(calls MockedCalls, offsetInterval ...time.Duration) MockFnMultiCall {
	w.suite.Helper()
	w.mockFnCall.AfterAll(calls, offsetInterval...)
	return w
}
func (w *mockFnMultiCall) Waiting(timeout time.Duration) MockFnMultiCall {
	w.mockFnCall.Waiting(timeout)
	return w
}
func (w *mockFnMultiCall) Bind(token *MockedCall) MockedCalls {
	w.suite.Helper()
	w.mockFnCall.Bind(token)
	return &mockFnMultiCalls{w.suite, w.mockFnCall.calls()}
}
func (w *mockFnMultiCall) Repeat(times uint) MockFnMultiCalls {
	w.suite.Helper()
	if times < 2 {
		w.mockFn.suite.Fatalf(repeatNotPluralError)
		return nil
	}

	calls := make([]MockedCall, times)
	calls[0] = w
	for i := uint(1); i < times; i++ {
		calls[i] = &mockedCall{w.Copy()}
	}
	return &mockFnMultiCalls{w.suite, calls}
}

// -----

type MockFnMultiCallArgs interface {
	Returning(functionResults ...interface{}) MockFnMultiCallArgs
	Before(call MockedCall, offsetInterval ...time.Duration) MockFnMultiCallArgs
	BeforeAll(call MockedCalls, offsetInterval ...time.Duration) MockFnMultiCallArgs
	After(call MockedCall, offsetInterval ...time.Duration) MockFnMultiCallArgs
	AfterAll(call MockedCalls, offsetInterval ...time.Duration) MockFnMultiCallArgs
	Waiting(timeout time.Duration) MockFnMultiCallArgs
	Bind(token *MockedCall) MockedCalls
	Repeat(times uint) MockFnMultiCalls
	MockedCalls
}
type mockFnMultiCallArgs struct {
	*mockFnMultiCall
}

func (w *mockFnMultiCallArgs) Returning(functionResults ...interface{}) MockFnMultiCallArgs {
	w.mockFnCall.Returning(functionResults...)
	return w
}
func (w *mockFnMultiCallArgs) Before(call MockedCall, offsetInterval ...time.Duration) MockFnMultiCallArgs {
	w.suite.Helper()
	w.mockFnCall.Before(call, offsetInterval...)
	return w
}
func (w *mockFnMultiCallArgs) BeforeAll(calls MockedCalls, offsetInterval ...time.Duration) MockFnMultiCallArgs {
	w.suite.Helper()
	w.mockFnCall.BeforeAll(calls, offsetInterval...)
	return w
}
func (w *mockFnMultiCallArgs) After(call MockedCall, offsetInterval ...time.Duration) MockFnMultiCallArgs {
	w.suite.Helper()
	w.mockFnCall.After(call, offsetInterval...)
	return w
}
func (w *mockFnMultiCallArgs) AfterAll(calls MockedCalls, offsetInterval ...time.Duration) MockFnMultiCallArgs {
	w.suite.Helper()
	w.mockFnCall.AfterAll(calls, offsetInterval...)
	return w
}
func (w *mockFnMultiCallArgs) Waiting(timeout time.Duration) MockFnMultiCallArgs {
	w.mockFnCall.Waiting(timeout)
	return w
}
