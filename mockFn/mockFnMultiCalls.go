package mockFn

type MockFnMultiCalls interface {
	Bind(token *MockedCalls) MockedCalls
	MockedCalls
}
type mockFnMultiCalls struct {
	suite  MockSuite
	_calls []MockedCall
}

func (m *mockFnMultiCalls) Bind(token *MockedCalls) MockedCalls {
	m.suite.Helper()
	if token == nil {
		m.suite.Fatalf(bindNilMockedCallError)
		return nil
	}

	*token = m
	return m
}
func (m *mockFnMultiCalls) calls() []MockedCall {
	return m._calls
}
