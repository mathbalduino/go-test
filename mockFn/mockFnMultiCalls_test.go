package mockFn

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"reflect"
	"testing"
)

func TestMockFnMultiCalls(t *testing.T) {
	suite := &internal.TestSuite{t}

	suite.Run("Bind", func(suite *internal.TestSuite) {
		suite.Run("Should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {
				helperCalls += 1
			}}
			fnMultiCalls := &mockFnMultiCalls{suite: mockSuite}

			var call MockedCalls
			fnMultiCalls.Bind(&call)
			if helperCalls != 1 {
				suite.Fatalf("The 'Bind' method not called the MockSuite 'Helper' method")
			}
		})
		suite.Run("Should throw a failure if the given *MockedCall is nil", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {
				fatalfCalls += 1
				if msg != bindNilMockedCallError {
					suite.Fatalf("The 'Bind' method threw the wrong failure message when receiving nil *MockedCall")
				}
				if len(args) != 0 {
					suite.Fatalf("The 'Bind' method gave the wrong arguments to the MockSuite 'Fatalf' method")
				}
			}}
			fnMultiCalls := &mockFnMultiCalls{suite: mockSuite}

			fnMultiCalls.Bind(nil)
			if fatalfCalls != 1 {
				suite.Fatalf("The 'Bind' method not called the MockSuite 'Fatalf' method when receiving a nil *MockedCall")
			}
		})
		suite.Run("Should correctly fill the given pointer with the mockFn itself (since it satisfies the interface)", func(suite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{Helper_: func() {}}
			fnMultiCalls := &mockFnMultiCalls{suite: mockSuite}

			var token MockedCalls
			fnMultiCalls.Bind(&token)
			if token == nil || token.(*mockFnMultiCalls) != fnMultiCalls {
				suite.Fatalf("The 'Bind' method didn't filled the given pointer with the correct MockedCalls value")
			}
		})
		suite.Run("Should return itself, providing a builder-like interface", func(suite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{Helper_: func() {}}
			fnMultiCalls := &mockFnMultiCalls{suite: mockSuite}

			var call MockedCalls
			if fnMultiCalls.Bind(&call).(*mockFnMultiCalls) != fnMultiCalls {
				suite.Fatalf("The 'Bind' method returned a wrong value")
			}
		})
	})
	suite.Run("The 'calls' method should return the field '_calls'", func(suite *internal.TestSuite) {
		expectedCalls := []MockedCall{&mockedCall{}, &mockedCall{}}
		fnMultiCalls := &mockFnMultiCalls{_calls: expectedCalls}

		receivedCalls := fnMultiCalls.calls()
		if !reflect.DeepEqual(expectedCalls, receivedCalls) {
			suite.Fatalf("The 'calls' method returned an unexpected value")
		}
	})
}
