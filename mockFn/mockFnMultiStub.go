package mockFn

type MockFnMultiStub interface {
	Repeat(times uint) MockedCalls
	MockedCalls
}
type mockFnMultiStub struct {
	*mockFnMultiCall
}

func (m *mockFnMultiStub) Repeat(times uint) MockedCalls {
	m.suite.Helper()
	m.mockFnMultiCall.Repeat(times)
	return m
}
