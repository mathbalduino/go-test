package mockFn

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"reflect"
	"testing"
	"time"
)

func TestMockFnMulti(t *testing.T) {
	tSuite := &internal.TestSuite{t}

	tSuite.Run("IgnoringArgs", func(tSuite *internal.TestSuite) {
		tSuite.Run("Should call the call factory", func(tSuite *internal.TestSuite) {
			factoryCalls := 0
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					factoryCalls += 1
					return &mockMockCall{_IgnoringArgs: func() {}}
				},
			}}

			mockMulti.IgnoringArgs()
			if factoryCalls != 1 {
				tSuite.Fatalf("The 'IgnoringArgs' method didn't called the call factory")
			}
		})
		tSuite.Run("Should forward the 'MockSuite' field value to the call factory", func(tSuite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{}
			mockMulti := &mockFnMulti{&mockFn{
				suite: mockSuite,
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					if suite == nil || suite.(*internal.MockSuite) != mockSuite {
						tSuite.Fatalf("The 'IgnoringArgs' method didn't forwarded the correct MockSuite to the call factory")
					}
					return &mockMockCall{_IgnoringArgs: func() {}}
				},
			}}
			mockMulti.IgnoringArgs()
		})
		tSuite.Run("Should forward the 'mockSkipFrame' field value to the call factory", func(tSuite *internal.TestSuite) {
			mockSkipFrame := uint(5)
			mockMulti := &mockFnMulti{&mockFn{
				mockSkipFrame: mockSkipFrame,
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					if len(skipFrames) != 1 || skipFrames[0] != mockSkipFrame {
						tSuite.Fatalf("The 'IgnoringArgs' method didn't forwarded the correct 'mockSkipFrame' field value to the call factory")
					}
					return &mockMockCall{_IgnoringArgs: func() {}}
				},
			}}
			mockMulti.IgnoringArgs()
		})
		tSuite.Run("The created call should call it's 'IgnoringArgs' method too", func(tSuite *internal.TestSuite) {
			ignoringArgsCalls := 0
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_IgnoringArgs: func() {
						ignoringArgsCalls += 1
					}}
				},
			}}
			mockMulti.IgnoringArgs()

			if ignoringArgsCalls != 1 {
				tSuite.Fatalf("The 'IgnoringArgs' method didn't correctly called the MockCall 'IgnoringArgs'")
			}
		})
		tSuite.Run("The 'syncCalls' and 'asyncCalls' fields should not be touched", func(tSuite *internal.TestSuite) {
			syncCalls := []MockedCall{nil, nil, nil}
			asyncCalls := []MockedCall{nil, nil}
			mockFn := &mockFn{
				syncCalls:  syncCalls,
				asyncCalls: asyncCalls,
				factory:    func(suite MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_IgnoringArgs: func() {}} },
			}
			mockMulti := &mockFnMulti{mockFn}
			mockMulti.IgnoringArgs()

			if !reflect.DeepEqual(syncCalls, mockFn.syncCalls) || !reflect.DeepEqual(asyncCalls, mockFn.asyncCalls) {
				tSuite.Fatalf("The 'IgnoringArgs' method ended up touching the 'syncCalls' or 'asyncCalls' field (or both)")
			}
		})
		tSuite.Run("Never returns a nil value", func(tSuite *internal.TestSuite) {
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_IgnoringArgs: func() {}} },
			}}

			if mockMulti.IgnoringArgs() == nil {
				tSuite.Fatalf("The 'IgnoringArgs' method returned a nil value")
			}
		})
		tSuite.Run("Returns the MockCall created by the factory", func(tSuite *internal.TestSuite) {
			correctMockCalled := 0
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{
						_IgnoringArgs: func() {},
						_Waiting: func(timeout time.Duration) {
							correctMockCalled += 1
						},
					}
				},
			}}

			mockMulti.IgnoringArgs()
			if correctMockCalled > 0 {
				tSuite.Fatalf("The 'IgnoringArgs' method returned a call that was not created by the call factory")
			}
		})
	})
	tSuite.Run("Expecting", func(tSuite *internal.TestSuite) {
		tSuite.Run("Should call the call factory", func(tSuite *internal.TestSuite) {
			factoryCalls := 0
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					factoryCalls += 1
					return &mockMockCall{_Expecting: func(expectedArgs ...interface{}) {}}
				},
			}}

			mockMulti.Expecting()
			if factoryCalls != 1 {
				tSuite.Fatalf("The 'Expecting' method didn't called the call factory")
			}
		})
		tSuite.Run("Should forward the 'MockSuite' field value to the call factory", func(tSuite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{}
			mockMulti := &mockFnMulti{&mockFn{
				suite: mockSuite,
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					if suite == nil || suite.(*internal.MockSuite) != mockSuite {
						tSuite.Fatalf("The 'Expecting' method didn't forwarded the correct MockSuite to the call factory")
					}
					return &mockMockCall{_Expecting: func(expectedArgs ...interface{}) {}}
				},
			}}
			mockMulti.Expecting()
		})
		tSuite.Run("Should forward the 'mockSkipFrame' field value to the call factory", func(tSuite *internal.TestSuite) {
			mockSkipFrame := uint(5)
			mockMulti := &mockFnMulti{&mockFn{
				mockSkipFrame: mockSkipFrame,
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					if len(skipFrames) != 1 || skipFrames[0] != mockSkipFrame {
						tSuite.Fatalf("The 'Expecting' method didn't forwarded the correct 'mockSkipFrame' field value to the call factory")
					}
					return &mockMockCall{_Expecting: func(expectedArgs ...interface{}) {}}
				},
			}}
			mockMulti.Expecting()
		})
		tSuite.Run("The created call should call it's 'Expecting' method too", func(tSuite *internal.TestSuite) {
			expectingCalls := 0
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_Expecting: func(expectedArgs ...interface{}) {
						expectingCalls += 1
					}}
				},
			}}
			mockMulti.Expecting()

			if expectingCalls != 1 {
				tSuite.Fatalf("The 'Expecting' method didn't correctly called the MockCall 'Expecting'")
			}
		})
		tSuite.Run("Should forward the arguments to the MockCall 'Expecting' method", func(tSuite *internal.TestSuite) {
			args := []interface{}{1, "value"}
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_Expecting: func(expectedArgs ...interface{}) {
						if !reflect.DeepEqual(expectedArgs, args) {
							suite.Fatalf("The 'Expecting' method didn't forwarded the correct arguments")
						}
					}}
				},
			}}
			mockMulti.Expecting(args...)
		})
		tSuite.Run("The 'syncCalls' and 'asyncCalls' fields should not be touched", func(tSuite *internal.TestSuite) {
			syncCalls := []MockedCall{nil, nil, nil}
			asyncCalls := []MockedCall{nil, nil}
			mockFn := &mockFn{
				syncCalls:  syncCalls,
				asyncCalls: asyncCalls,
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_Expecting: func(expectedArgs ...interface{}) {}}
				},
			}
			mockMulti := &mockFnMulti{mockFn}
			mockMulti.Expecting()

			if !reflect.DeepEqual(syncCalls, mockFn.syncCalls) || !reflect.DeepEqual(asyncCalls, mockFn.asyncCalls) {
				tSuite.Fatalf("The 'Expecting' method ended up touching the 'syncCalls' or 'asyncCalls' field (or both)")
			}
		})
		tSuite.Run("Never returns a nil value", func(tSuite *internal.TestSuite) {
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_Expecting: func(expectedArgs ...interface{}) {}}
				},
			}}

			if mockMulti.Expecting() == nil {
				tSuite.Fatalf("The 'Expecting' method returned a nil value")
			}
		})
		tSuite.Run("Returns the MockCall created by the factory", func(tSuite *internal.TestSuite) {
			correctMockCalled := 0
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{
						_Expecting: func(expectedArgs ...interface{}) {},
						_Waiting: func(timeout time.Duration) {
							correctMockCalled += 1
						},
					}
				},
			}}

			mockMulti.Expecting().Waiting(time.Second)
			if correctMockCalled != 1 {
				tSuite.Fatalf("The 'Expecting' method returned a call that was not created by the call factory")
			}
		})
	})
	tSuite.Run("AsStub", func(tSuite *internal.TestSuite) {
		tSuite.Run("Should call the call factory", func(tSuite *internal.TestSuite) {
			factoryCalls := 0
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					factoryCalls += 1
					return &mockMockCall{_AsStub: func(expectedResults ...interface{}) {}}
				},
			}}

			mockMulti.AsStub()
			if factoryCalls != 1 {
				tSuite.Fatalf("The 'AsStub' method didn't called the call factory")
			}
		})
		tSuite.Run("Should forward the 'MockSuite' field value to the call factory", func(tSuite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{}
			mockMulti := &mockFnMulti{&mockFn{
				suite: mockSuite,
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					if suite == nil || suite.(*internal.MockSuite) != mockSuite {
						tSuite.Fatalf("The 'AsStub' method didn't forwarded the correct MockSuite to the call factory")
					}
					return &mockMockCall{_AsStub: func(expectedResults ...interface{}) {}}
				},
			}}
			mockMulti.AsStub()
		})
		tSuite.Run("Should forward the 'mockSkipFrame' field value to the call factory", func(tSuite *internal.TestSuite) {
			mockSkipFrame := uint(5)
			mockMulti := &mockFnMulti{&mockFn{
				mockSkipFrame: mockSkipFrame,
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					if len(skipFrames) != 1 || skipFrames[0] != mockSkipFrame {
						tSuite.Fatalf("The 'AsStub' method didn't forwarded the correct 'mockSkipFrame' field value to the call factory")
					}
					return &mockMockCall{_AsStub: func(expectedResults ...interface{}) {}}
				},
			}}
			mockMulti.AsStub()
		})
		tSuite.Run("The created call should call it's 'AsStub' method too", func(tSuite *internal.TestSuite) {
			expectingCalls := 0
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_AsStub: func(expectedResults ...interface{}) {
						expectingCalls += 1
					}}
				},
			}}
			mockMulti.AsStub()

			if expectingCalls != 1 {
				tSuite.Fatalf("The 'AsStub' method didn't correctly called the MockCall 'AsStub'")
			}
		})
		tSuite.Run("Should forward the results to the MockCall 'AsStub' method", func(tSuite *internal.TestSuite) {
			results := []interface{}{1, "value"}
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_AsStub: func(expectedResults ...interface{}) {
						if !reflect.DeepEqual(expectedResults, results) {
							suite.Fatalf("The 'AsStub' method didn't forwarded the correct results")
						}
					}}
				},
			}}
			mockMulti.AsStub(results...)
		})
		tSuite.Run("The 'syncCalls' and 'asyncCalls' fields should not be touched", func(tSuite *internal.TestSuite) {
			syncCalls := []MockedCall{nil, nil, nil}
			asyncCalls := []MockedCall{nil, nil}
			mockFn := &mockFn{
				syncCalls:  syncCalls,
				asyncCalls: asyncCalls,
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_AsStub: func(expectedResults ...interface{}) {}}
				},
			}
			mockMulti := &mockFnMulti{mockFn}
			mockMulti.AsStub()

			if !reflect.DeepEqual(syncCalls, mockFn.syncCalls) || !reflect.DeepEqual(asyncCalls, mockFn.asyncCalls) {
				tSuite.Fatalf("The 'AsStub' method ended up touching the 'syncCalls' or 'asyncCalls' field (or both)")
			}
		})
		tSuite.Run("Never returns a nil value", func(tSuite *internal.TestSuite) {
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_AsStub: func(expectedResults ...interface{}) {}}
				},
			}}

			if mockMulti.AsStub() == nil {
				tSuite.Fatalf("The 'AsStub' method returned a nil value")
			}
		})
		tSuite.Run("Returns the MockCall created by the factory", func(tSuite *internal.TestSuite) {
			call := &mockMockCall{_AsStub: func(expectedResults ...interface{}) {}}
			mockMulti := &mockFnMulti{&mockFn{
				factory: func(suite MockSuite, skipFrames ...uint) iMockCall {					return call				},
			}}

			receivedCall := mockMulti.AsStub().(*mockFnMultiStub)
			if receivedCall.iMockCall.(*mockMockCall) != call {
				tSuite.Fatalf("The 'AsStub' method returned a call that was not created by the call factory")
			}
		})
	})
}
