package mockFn

type MockFnStub interface {
	Repeat(times uint) MockedFn
	MockedFn
}
type mockFnStub struct {
	*mockFnCall
}

func (m *mockFnStub) Repeat(times uint) MockedFn {
	m.suite.Helper()
	m.mockFnCall.Repeat(times)
	return m
}
