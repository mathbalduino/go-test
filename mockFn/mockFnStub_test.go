package mockFn

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"testing"
)

func TestMockFnStub(t *testing.T) {
	suite := &internal.TestSuite{t}

	suite.Run("The 'Repeat' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockFnStub := &mockFnStub{&mockFnCall{
			mockFn: &mockFn{suite: &internal.MockSuite{
				Helper_: func() {
					helperCalls += 1
				},
				Fatalf_: func(msg string, args ...interface{}) {},
			}},
		}}

		mockFnStub.Repeat(0)  // shorten the test expecting that "0" will throw failure
		if helperCalls != 2 { // expect that the underlying mockFnCall will call the helper too
			suite.Fatalf("The 'Repeat' method didn't call the MockSuite 'Helper' method")
		}
	})
}
