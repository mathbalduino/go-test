package mockFn

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"reflect"
	"testing"
)

func TestNewMockFn(t *testing.T) {
	suite := &internal.TestSuite{t}

	suite.Run("Never returns a nil pointer", func(suite *internal.TestSuite) {
		if NewMockFn(suite) == nil {
			suite.Fatalf("The 'NewMockFn' function returned a nil pointer")
		}
	})
	suite.Run("Should set the 'MockSuite' field correctly", func(suite *internal.TestSuite) {
		mock := NewMockFn(suite).(*mockFn)

		if mock.suite == nil || mock.suite.(*internal.TestSuite) != suite {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with the correct 'MockSuite' field")
		}
	})
	suite.Run("Should set the 'fileLine' field correctly", func(suite *internal.TestSuite) {
		mock, fileLine := NewMockFn(suite).(*mockFn), internal.FnCallFileLine(0)
		if mock.fileLine != fileLine {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with the correct 'fileLine' field")
		}
	})
	suite.Run("Should set the 'fileLine' field correctly (with skipFrames set)", func(suite *internal.TestSuite) {
		mock, fileLine := NewMockFn(suite, 1).(*mockFn), internal.FnCallFileLine(1)
		if mock.fileLine != fileLine {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with the correct 'fileLine' field (with one skipFrames value)")
		}
	})
	suite.Run("Should set the field 'mockSkipFrame' to the default value (one) if not provided", func(suite *internal.TestSuite) {
		mock := NewMockFn(suite).(*mockFn)

		if mock.mockSkipFrame != 1 {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with the correct 'mockSkipFrame' value (without skipFrames)")
		}
	})
	suite.Run("Should set the field 'mockSkipFrame' incrementing the provided value by one", func(suite *internal.TestSuite) {
		providedValue := uint(1)
		mock := NewMockFn(suite, providedValue).(*mockFn)

		if mock.mockSkipFrame != 1+providedValue {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with the correct 'mockSkipFrame' value (with custom skipFrames)")
		}
	})
	suite.Run("Should set the field 'syncCalls' to nil", func(suite *internal.TestSuite) {
		mock := NewMockFn(suite).(*mockFn)

		if mock.syncCalls != nil {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with the 'syncCalls' slice nil")
		}
	})
	suite.Run("Should set the field 'asyncCalls' to nil", func(suite *internal.TestSuite) {
		mock := NewMockFn(suite).(*mockFn)

		if mock.asyncCalls != nil {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with the 'asyncCalls' slice nil")
		}
	})
	suite.Run("Should set the field 'nextCall' to zero", func(suite *internal.TestSuite) {
		mock := NewMockFn(suite).(*mockFn)

		if mock.nextCall != 0 {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with the 'nextCall' set to zero")
		}
	})
	suite.Run("The 'factory' field should not be nil", func(suite *internal.TestSuite) {
		mock := NewMockFn(suite).(*mockFn)

		if mock == nil || mock.factory == nil {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with a valid 'factory' field")
		}
	})
	suite.Run("The 'factory' field should not return a nil iMockCall", func(suite *internal.TestSuite) {
		mock := NewMockFn(suite).(*mockFn)

		if mock == nil || mock.factory == nil || mock.factory(suite) == nil {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with a 'factory' field that returns a valid iMockCall")
		}
	})
	suite.Run("Should set the field 'syncCallsCalled' to false", func(suite *internal.TestSuite) {
		mock := NewMockFn(suite).(*mockFn)

		if mock.syncCallsCalled {
			suite.Fatalf("The 'NewMockFn' function didn't returned a MockFn with the 'syncCallsCalled' set to false")
		}
	})
	suite.Run("Should call the 'Cleanup' method of the MockSuite with the 'cleanupChecks' method as argument", func(suite *internal.TestSuite) {
		var capturedCleanup func()
		mockSuite := &internal.MockSuite{
			Cleanup_: func(f func()) {
				capturedCleanup = f
			},
		}
		mock := NewMockFn(mockSuite).(*mockFn)

		// Rely on the fact that the 'cleanupChecks' method will call the 'wait' method of every
		// registered call in the 'syncCalls' and 'asyncCalls' fields
		waitCalls := 0
		call := &mockMockedCall{
			wait_: func() {
				waitCalls += 1
			},
		}
		mock.syncCalls = []MockedCall{call}
		capturedCleanup()
		if waitCalls != 1 {
			suite.Fatalf("The 'NewMockFn' function is not passing a correct callback to the 'Cleanup' method of the MockSuite")
		}
	})
}

func TestMockFn(t *testing.T) {
	tSuite := &internal.TestSuite{t}

	tSuite.Run("IgnoringArgs", func(tSuite *internal.TestSuite) {
		tSuite.Run("Should call the 'factory' field", func(tSuite *internal.TestSuite) {
			factoryCalls := 0
			mock := &mockFn{
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					factoryCalls += 1
					return &mockMockCall{_IgnoringArgs: func() {}}
				},
			}

			mock.IgnoringArgs()
			if factoryCalls != 1 {
				tSuite.Fatalf("The 'IgnoringArgs' method is not correctly calling the 'factory' field")
			}
		})
		tSuite.Run("Should forward the 'MockSuite' to the 'factory' field, when calling it", func(tSuite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{}
			mock := &mockFn{
				suite: mockSuite,
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					if s == nil || s.(*internal.MockSuite) != mockSuite {
						tSuite.Fatalf("The 'IgnoringArgs' is not forwarding the correct 'MockSuite' value to the created call")
					}

					return &mockMockCall{_IgnoringArgs: func() {}}
				},
			}

			mock.IgnoringArgs()
		})
		tSuite.Run("Should forward the 'mockSkipFrame' to the 'factory' field, when calling it", func(tSuite *internal.TestSuite) {
			mockSkipFrame := uint(10)
			mock := &mockFn{
				mockSkipFrame: mockSkipFrame,
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					if len(skipFrames) != 1 || skipFrames[0] != mockSkipFrame {
						tSuite.Fatalf("The 'IgnoringArgs' is not forwarding the correct 'MockSuite' value to the created call")
					}

					return &mockMockCall{_IgnoringArgs: func() {}}
				},
			}

			mock.IgnoringArgs()
		})
		tSuite.Run("The created call should call it's 'IgnoringArgs' method too", func(tSuite *internal.TestSuite) {
			ignoringArgsCalls := 0
			mock := &mockFn{
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_IgnoringArgs: func() {
						ignoringArgsCalls += 1
					}}
				},
			}

			mock.IgnoringArgs()
			if ignoringArgsCalls != 1 {
				tSuite.Fatalf("The 'IgnoringArgs' method is not correctly calling the MockCall 'IgnoringArgs' method")
			}
		})
		tSuite.Run("The 'syncCalls' field should be set to a singleton with only the current created call", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				factory:   func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_IgnoringArgs: func() {}} },
				syncCalls: []MockedCall{nil, nil, nil, nil}, // len was 4
			}

			call := mock.IgnoringArgs().(*mockFnCallArgs)
			if len(call.syncCalls) != 1 || call.syncCalls[0].(*mockedCall) != call.mockedCall {
				tSuite.Fatalf("The 'IgnoringArgs' is not setting the 'syncCalls' to a singleton with the newly created call")
			}
		})
		tSuite.Run("The 'asyncCalls' field should be set to nil", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				factory:    func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_IgnoringArgs: func() {}} },
				asyncCalls: []MockedCall{nil, nil, nil, nil}, // len was 4
			}

			call := mock.IgnoringArgs().(*mockFnCallArgs)
			if call.asyncCalls != nil {
				tSuite.Fatalf("The 'IgnoringArgs' is not setting the 'asyncCalls' to nil")
			}
		})
		tSuite.Run("Set the 'syncCallsCalled' field to false", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				syncCallsCalled: true,
				factory: func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_IgnoringArgs: func() {}} },
			}

			mock.IgnoringArgs()
			if mock.syncCallsCalled {
				tSuite.Fatalf("The 'IgnoringArgs' method didn't set the 'syncCallsCalled' field to false")
			}
		})
		tSuite.Run("Never returns a nil MockFnArgs", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				factory: func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_IgnoringArgs: func() {}} },
			}

			if mock.IgnoringArgs() == nil {
				tSuite.Fatalf("The 'IgnoringArgs' is returning a nil value")
			}
		})
	})
	tSuite.Run("Expecting", func(tSuite *internal.TestSuite) {
		tSuite.Run("Should call the 'factory' field", func(tSuite *internal.TestSuite) {
			factoryCalls := 0
			mock := &mockFn{
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					factoryCalls += 1
					return &mockMockCall{_Expecting: func(...interface{}) {}}
				},
			}

			mock.Expecting()
			if factoryCalls != 1 {
				tSuite.Fatalf("The 'Expecting' method is not correctly calling the 'factory' field")
			}
		})
		tSuite.Run("Should forward the 'MockSuite' to the 'factory' field, when calling it", func(tSuite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{}
			mock := &mockFn{
				suite: mockSuite,
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					if s == nil || s.(*internal.MockSuite) != mockSuite {
						tSuite.Fatalf("The 'Expecting' is not forwarding the correct 'MockSuite' value to the created call")
					}

					return &mockMockCall{_Expecting: func(...interface{}) {}}
				},
			}

			mock.Expecting()
		})
		tSuite.Run("Should forward the 'mockSkipFrame' to the 'factory' field, when calling it", func(tSuite *internal.TestSuite) {
			mockSkipFrame := uint(10)
			mock := &mockFn{
				mockSkipFrame: mockSkipFrame,
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					if len(skipFrames) != 1 || skipFrames[0] != mockSkipFrame {
						tSuite.Fatalf("The 'Expecting' is not forwarding the correct 'MockSuite' value to the created call")
					}

					return &mockMockCall{_Expecting: func(...interface{}) {}}
				},
			}

			mock.Expecting()
		})
		tSuite.Run("The created call should call it's 'Expecting' method too, forwarding the received arguments", func(tSuite *internal.TestSuite) {
			expectingCalls := 0
			args := []interface{}{1, "value"}
			mock := &mockFn{
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_Expecting: func(receivedArgs ...interface{}) {
						expectingCalls += 1
						if !reflect.DeepEqual(receivedArgs, args) {
							tSuite.Fatalf("The 'Expecting' method didn't forwarded the received arguments to the MockCall 'Expecting' method")
						}
					}}
				},
			}

			mock.Expecting(args...)
			if expectingCalls != 1 {
				tSuite.Fatalf("The 'Expecting' method is not correctly calling the MockCall 'Expecting' method")
			}
		})
		tSuite.Run("The 'syncCalls' field should be set to a singleton with only the current created call", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				factory:   func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_Expecting: func(...interface{}) {}} },
				syncCalls: []MockedCall{nil, nil, nil, nil}, // len was 4
			}

			call := mock.Expecting().(*mockFnCall)
			if len(call.syncCalls) != 1 || call.syncCalls[0].(*mockedCall) != call.mockedCall {
				tSuite.Fatalf("The 'Expecting' is not setting the 'syncCalls' to a singleton with the newly created call")
			}
		})
		tSuite.Run("The 'asyncCalls' field should be set to nil", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				factory:    func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_Expecting: func(...interface{}) {}} },
				asyncCalls: []MockedCall{nil, nil, nil, nil}, // len was 4
			}

			call := mock.Expecting().(*mockFnCall)
			if call.asyncCalls != nil {
				tSuite.Fatalf("The 'Expecting' is not setting the 'asyncCalls' to nil")
			}
		})
		tSuite.Run("Set the 'syncCallsCalled' field to false", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				syncCallsCalled: true,
				factory: func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_Expecting: func(...interface{}) {}} },
			}

			mock.Expecting()
			if mock.syncCallsCalled {
				tSuite.Fatalf("The 'Expecting' method didn't set the 'syncCallsCalled' field to false")
			}
		})
		tSuite.Run("Never returns a nil MockFnArgs", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				factory: func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_Expecting: func(...interface{}) {}} },
			}

			if mock.Expecting() == nil {
				tSuite.Fatalf("The 'Expecting' is returning a nil value")
			}
		})
	})
	tSuite.Run("AsStub", func(tSuite *internal.TestSuite) {
		tSuite.Run("Should call the 'factory' field", func(tSuite *internal.TestSuite) {
			factoryCalls := 0
			mock := &mockFn{
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					factoryCalls += 1
					return &mockMockCall{_AsStub: func(...interface{}) {}}
				},
			}

			mock.AsStub()
			if factoryCalls != 1 {
				tSuite.Fatalf("The 'AsStub' method is not correctly calling the 'factory' field")
			}
		})
		tSuite.Run("Should forward the 'MockSuite' to the 'factory' field, when calling it", func(tSuite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{}
			mock := &mockFn{
				suite: mockSuite,
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					if s == nil || s.(*internal.MockSuite) != mockSuite {
						tSuite.Fatalf("The 'AsStub' is not forwarding the correct 'MockSuite' value to the created call")
					}

					return &mockMockCall{_AsStub: func(...interface{}) {}}
				},
			}

			mock.AsStub()
		})
		tSuite.Run("Should forward the 'mockSkipFrame' to the 'factory' field, when calling it", func(tSuite *internal.TestSuite) {
			mockSkipFrame := uint(10)
			mock := &mockFn{
				mockSkipFrame: mockSkipFrame,
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					if len(skipFrames) != 1 || skipFrames[0] != mockSkipFrame {
						tSuite.Fatalf("The 'AsStub' is not forwarding the correct 'MockSuite' value to the created call")
					}

					return &mockMockCall{_AsStub: func(...interface{}) {}}
				},
			}

			mock.AsStub()
		})
		tSuite.Run("The created call should call it's 'AsStub' method too, forwarding the received results", func(tSuite *internal.TestSuite) {
			expectingCalls := 0
			results := []interface{}{1, "value"}
			mock := &mockFn{
				factory: func(s MockSuite, skipFrames ...uint) iMockCall {
					return &mockMockCall{_AsStub: func(receivedResults ...interface{}) {
						expectingCalls += 1
						if !reflect.DeepEqual(receivedResults, results) {
							tSuite.Fatalf("The 'AsStub' method didn't forwarded the received results to the MockCall 'AsStub' method")
						}
					}}
				},
			}

			mock.AsStub(results...)
			if expectingCalls != 1 {
				tSuite.Fatalf("The 'AsStub' method is not correctly calling the MockCall 'AsStub' method")
			}
		})
		tSuite.Run("The 'syncCalls' field should be set to a singleton with only the current created call", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				factory:   func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_AsStub: func(...interface{}) {}} },
				syncCalls: []MockedCall{nil, nil, nil, nil}, // len was 4
			}

			call := mock.AsStub().(*mockFnStub)
			if len(call.syncCalls) != 1 || call.syncCalls[0].(*mockedCall) != call.mockedCall {
				tSuite.Fatalf("The 'AsStub' is not setting the 'syncCalls' to a singleton with the newly created call")
			}
		})
		tSuite.Run("The 'asyncCalls' field should be set to nil", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				factory:    func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_AsStub: func(...interface{}) {}} },
				asyncCalls: []MockedCall{nil, nil, nil, nil}, // len was 4
			}

			call := mock.AsStub().(*mockFnStub)
			if call.asyncCalls != nil {
				tSuite.Fatalf("The 'AsStub' is not setting the 'asyncCalls' to nil")
			}
		})
		tSuite.Run("Set the 'syncCallsCalled' field to false", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				syncCallsCalled: true,
				factory: func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_AsStub: func(...interface{}) {}} },
			}

			mock.AsStub()
			if mock.syncCallsCalled {
				tSuite.Fatalf("The 'AsStub' method didn't set the 'syncCallsCalled' field to false")
			}
		})
		tSuite.Run("Never returns a nil MockFnArgs", func(tSuite *internal.TestSuite) {
			mock := &mockFn{
				factory: func(s MockSuite, skipFrames ...uint) iMockCall { return &mockMockCall{_AsStub: func(...interface{}) {}} },
			}

			if mock.AsStub() == nil {
				tSuite.Fatalf("The 'AsStub' is returning a nil value")
			}
		})
	})
	tSuite.Run("SyncCalls", func(tSuite *internal.TestSuite) {
		tSuite.Run("Should call the MockSuite 'Helper' method", func(tSuite *internal.TestSuite) {
			helperCalls := 0
			mockFn := &mockFn{
				suite: &internal.MockSuite{
					Helper_: func() { helperCalls += 1 },

					// On purpose this test can throw a failure, just to shorten the test
					Fatalf_: func(msg string, args ...interface{}) {},
				},
			}

			mockFn.SyncCalls(func(builder MockFnMulti) SequentialCalls { return nil })
			if helperCalls != 1 {
				tSuite.Fatalf("The 'SyncCalls' method didn't called the MockSuite 'Helper' method")
			}
		})
		tSuite.Run("Should throw a failure if the callback is nil", func(tSuite *internal.TestSuite) {
			fatalfCalls := 0
			mockFn := &mockFn{
				suite: &internal.MockSuite{
					Helper_: func() {},
					Fatalf_: func(msg string, args ...interface{}) {
						fatalfCalls += 1
						if msg != syncNilCallbackError {
							tSuite.Fatalf("The 'SyncCalls' method not used the correct failure message to signal that the callback is nil")
						}
						if len(args) != 0 {
							tSuite.Fatalf("The 'SyncCalls' method used unnecessary arguments to the MockSuite 'Fatalf' method")
						}
					},
				},
			}

			mockFn.SyncCalls(nil)
			if fatalfCalls != 1 {
				tSuite.Fatalf("The 'SyncCalls' method didn't threw a failure when the callback is nil")
			}
		})
		tSuite.Run("Should ensure that the callback is called", func(tSuite *internal.TestSuite) {
			mockFn := &mockFn{
				suite: &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {}},
			}

			callbackCalls := 0
			mockFn.SyncCalls(func(builder MockFnMulti) SequentialCalls {
				callbackCalls += 1
				return nil
			})
			if callbackCalls != 1 {
				tSuite.Fatalf("The 'SyncCalls' didn't called the received callback")
			}
		})
		tSuite.Run("Should set the 'syncCalls' and 'asyncCalls' fields to nil before calling the callback", func(tSuite *internal.TestSuite) {
			mockFn := &mockFn{
				suite:      &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {}},
				syncCalls:  []MockedCall{nil, nil, nil, nil}, // len was 4
				asyncCalls: []MockedCall{nil, nil, nil, nil}, // len was 4
			}

			mockFn.SyncCalls(func(builder MockFnMulti) SequentialCalls {
				if mockFn.syncCalls != nil {
					tSuite.Fatalf("The 'SyncCalls' is not setting the 'syncCalls' to nil before calling the callback")
				}
				if mockFn.asyncCalls != nil {
					tSuite.Fatalf("The 'SyncCalls' is not setting the 'asyncCalls' to nil before calling the callback")
				}

				return nil
			})
		})
		tSuite.Run("Should throw a failure if the callback doesn't returns more than one call", func(tSuite *internal.TestSuite) {
			fatalfCalls := 0
			mockFn := &mockFn{
				suite: &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != syncSingleCallError {
						tSuite.Fatalf("The 'SyncCalls' method didn't use the correct failure message to signal that the callback didn't returns more than one call")
					}
					if len(args) != 0 {
						tSuite.Fatalf("The 'SyncCalls' method gave wrong arguments to the MockSuite 'Fatalf' method")
					}
				}},
			}

			mockFn.SyncCalls(func(builder MockFnMulti) SequentialCalls { return nil })
			if fatalfCalls != 1 {
				tSuite.Fatalf("The 'SyncCalls' method didn't throw failure when the callback didn't returns more than one call")
			}
			mockFn.SyncCalls(func(builder MockFnMulti) SequentialCalls { return SequentialCalls{} })
			if fatalfCalls != 2 {
				tSuite.Fatalf("The 'SyncCalls' method didn't throw failure when the callback didn't returns more than one call")
			}
			mockFn.SyncCalls(func(builder MockFnMulti) SequentialCalls { return SequentialCalls{nil} })
			if fatalfCalls != 3 {
				tSuite.Fatalf("The 'SyncCalls' method didn't throw failure when the callback didn't returns more than one call")
			}
		})
		tSuite.Run("Should set the 'syncCallsCalled' field to true before returning", func(tSuite *internal.TestSuite) {
			mock := &mockFn{suite: &internal.MockSuite{Helper_: func() {}}}

			mock.SyncCalls(func(builder MockFnMulti) SequentialCalls { return SequentialCalls{&mockFnMultiCalls{}, &mockFnMultiCalls{}} })
			if !mock.syncCallsCalled {
				tSuite.Fatalf("The 'SyncCalls' didn't set the 'syncCallsCalled' boolean to true")
			}
		})
		tSuite.Run("Should never return a nil MockFnAux", func(tSuite *internal.TestSuite) {
			mock := &mockFn{suite: &internal.MockSuite{Helper_: func() {}}}

			if mock.SyncCalls(func(builder MockFnMulti) SequentialCalls {
				return SequentialCalls{&mockFnMultiCalls{}, &mockFnMultiCalls{}}
			}) == nil {
				tSuite.Fatalf("The 'SyncCalls' returned a nil MockFnAux")
			}
		})
	})
	tSuite.Run("AsyncCalls", func(tSuite *internal.TestSuite) {
		tSuite.Run("Should call the MockSuite 'Helper' method", func(tSuite *internal.TestSuite) {
			helperCalls := 0
			mockFn := &mockFn{
				suite: &internal.MockSuite{
					Helper_: func() { helperCalls += 1 },

					// On purpose this test can throw a failure, just to shorten the test
					Fatalf_: func(msg string, args ...interface{}) {},
				},
			}

			mockFn.AsyncCalls(func(builder MockFnMulti) SequentialCalls { return nil })
			if helperCalls != 1 {
				tSuite.Fatalf("The 'AsyncCalls' method didn't called the MockSuite 'Helper' method")
			}
		})
		tSuite.Run("Should throw a failure if the callback is nil", func(tSuite *internal.TestSuite) {
			fatalfCalls := 0
			mockFn := &mockFn{
				suite: &internal.MockSuite{
					Helper_: func() {},
					Fatalf_: func(msg string, args ...interface{}) {
						fatalfCalls += 1
						if msg != asyncNilCallbackError {
							tSuite.Fatalf("The 'AsyncCalls' method not used the correct failure message to signal that the callback is nil")
						}
						if len(args) != 0 {
							tSuite.Fatalf("The 'AsyncCalls' method used unnecessary arguments to the MockSuite 'Fatalf' method")
						}
					},
				},
			}

			mockFn.AsyncCalls(nil)
			if fatalfCalls != 1 {
				tSuite.Fatalf("The 'AsyncCalls' method didn't threw a failure when the callback is nil")
			}
		})
		tSuite.Run("Should ensure that the callback is called", func(tSuite *internal.TestSuite) {
			mockFn := &mockFn{
				suite: &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {}},
			}

			callbackCalls := 0
			mockFn.AsyncCalls(func(builder MockFnMulti) SequentialCalls {
				callbackCalls += 1
				return nil
			})
			if callbackCalls != 1 {
				tSuite.Fatalf("The 'AsyncCalls' didn't called the received callback")
			}
		})
		tSuite.Run("Should set the 'asyncCalls' field to nil before calling the callback", func(tSuite *internal.TestSuite) {
			mockFn := &mockFn{
				suite:      &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {}},
				asyncCalls: []MockedCall{nil, nil, nil, nil}, // len was 4
			}

			mockFn.AsyncCalls(func(builder MockFnMulti) SequentialCalls {
				if mockFn.asyncCalls != nil {
					tSuite.Fatalf("The 'AsyncCalls' is not setting the 'asyncCalls' to nil before calling the callback")
				}

				return nil
			})
		})
		tSuite.Run("Should set the 'syncCalls' field to nil only if the 'SyncCalls' method was not called", func(tSuite *internal.TestSuite) {
			mockFn := &mockFn{
				suite:      &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {}},
				syncCallsCalled: false,
				syncCalls:  []MockedCall{nil, nil, nil, nil}, // len was 4
			}

			mockFn.AsyncCalls(func(builder MockFnMulti) SequentialCalls {
				if mockFn.syncCalls != nil {
					tSuite.Fatalf("The 'AsyncCalls' is not setting the 'syncCalls' to nil before calling the callback")
				}

				return nil
			})

			syncCalls := []MockedCall{nil, nil, nil, nil}
			mockFn.syncCalls = syncCalls
			mockFn.syncCallsCalled = true
			mockFn.AsyncCalls(func(builder MockFnMulti) SequentialCalls {
				if !reflect.DeepEqual(mockFn.syncCalls, syncCalls) {
					tSuite.Fatalf("The 'AsyncCalls' is changing the 'syncCalls' field even when the 'SyncCalls' method was called")
				}

				return nil
			})
		})
		tSuite.Run("Should throw a failure if the callback doesn't returns more than one call", func(tSuite *internal.TestSuite) {
			fatalfCalls := 0
			mockFn := &mockFn{
				suite: &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != asyncSingleCallError {
						tSuite.Fatalf("The 'AsyncCalls' method didn't use the correct failure message to signal that the callback didn't returns more than one call")
					}
					if len(args) != 0 {
						tSuite.Fatalf("The 'AsyncCalls' method gave wrong arguments to the MockSuite 'Fatalf' method")
					}
				}},
			}

			mockFn.AsyncCalls(func(builder MockFnMulti) SequentialCalls { return nil })
			if fatalfCalls != 1 {
				tSuite.Fatalf("The 'AsyncCalls' method didn't throw failure when the callback didn't returns more than one call")
			}
			mockFn.AsyncCalls(func(builder MockFnMulti) SequentialCalls { return SequentialCalls{} })
			if fatalfCalls != 2 {
				tSuite.Fatalf("The 'AsyncCalls' method didn't throw failure when the callback didn't returns more than one call")
			}
			mockFn.AsyncCalls(func(builder MockFnMulti) SequentialCalls { return SequentialCalls{nil} })
			if fatalfCalls != 3 {
				tSuite.Fatalf("The 'AsyncCalls' method didn't throw failure when the callback didn't returns more than one call")
			}
		})
		tSuite.Run("Should never return a nil MockFnAux", func(tSuite *internal.TestSuite) {
			mock := &mockFn{suite: &internal.MockSuite{Helper_: func() {}}}

			if mock.AsyncCalls(func(builder MockFnMulti) SequentialCalls {
				return SequentialCalls{&mockFnMultiCalls{}, &mockFnMultiCalls{}}
			}) == nil {
				tSuite.Fatalf("The 'AsyncCalls' returned a nil MockFnAux")
			}
		})
	})
	tSuite.Run("Bind", func(tSuite *internal.TestSuite) {
		tSuite.Run("Should call the MockSuite 'Helper' method", func(tSuite *internal.TestSuite) {
			helperCalls := 0
			m := &mockFn{suite: &internal.MockSuite{
				Helper_: func() { helperCalls += 1 },
			}}

			var token MockedCalls
			m.Bind(&token)
			if helperCalls != 1 {
				tSuite.Fatalf("The 'Bind' method didn't call the MockSuite 'Helper' method")
			}
		})
		tSuite.Run("Should throw a failure if the argument is nil", func(tSuite *internal.TestSuite) {
			fatalfCalls := 0
			m := &mockFn{suite: &internal.MockSuite{
				Helper_: func() {},
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != bindNilMockedCallError {
						tSuite.Fatalf("The 'Bind' method didn't throw the correct failure message to signal that it's argument is nil")
					}
					if len(args) != 0 {
						tSuite.Fatalf("The 'Bind' method used unnecessary arguments when calling the MockSuite 'Fatalf' method to signal that it's argument is nil")
					}
				},
			}}

			m.Bind(nil)
			if fatalfCalls != 1 {
				tSuite.Fatalf("The 'Bind' method didn't throw a failure message to signal that it's argument is nil")
			}
		})
		tSuite.Run("Should correctly fill the given pointer with the mockFn itself (since it satisfies the interface)", func(tSuite *internal.TestSuite) {
			var token MockedCalls
			m := &mockFn{suite: &internal.MockSuite{Helper_: func() {}}}

			m.Bind(&token)
			if token == nil || token.(*mockFn) != m {
				tSuite.Fatalf("The 'Bind' method didn't filled the given pointer with the correct MockedCalls value")
			}
		})
		tSuite.Run("Should not return a nil MockedFn", func(tSuite *internal.TestSuite) {
			var token MockedCalls
			m := &mockFn{suite: &internal.MockSuite{Helper_: func() {}}}

			if m.Bind(&token) == nil {
				tSuite.Fatalf("The 'Bind' method returned a nil MockedFn")
			}
		})
	})
	tSuite.Run("'cleanupChecks' method should call the 'wait' method of every registered call (sync or async)", func(tSuite *internal.TestSuite) {
		syncWaitCalls := 0
		syncWait := func() {
			syncWaitCalls += 1
		}
		asyncWaitCalls := 0
		asyncWait := func() {
			asyncWaitCalls += 1
		}
		syncCalls := []MockedCall{
			&mockMockedCall{wait_: syncWait}, &mockMockedCall{wait_: syncWait}, &mockMockedCall{wait_: syncWait},
			&mockMockedCall{wait_: syncWait},
			&mockMockedCall{wait_: syncWait}, &mockMockedCall{wait_: syncWait},
		}
		asyncCalls := []MockedCall{
			&mockMockedCall{wait_: asyncWait}, &mockMockedCall{wait_: asyncWait},
			&mockMockedCall{wait_: asyncWait}, &mockMockedCall{wait_: asyncWait},
			&mockMockedCall{wait_: asyncWait}, &mockMockedCall{wait_: asyncWait}, &mockMockedCall{wait_: asyncWait},
		}
		mock := &mockFn{
			syncCalls:  syncCalls,
			asyncCalls: asyncCalls,
		}

		mock.cleanupChecks()
		if len(syncCalls) != syncWaitCalls {
			tSuite.Fatalf("The 'cleanupChecks' method is not calling the 'wait' method once for each 'syncCalls' element")
		}
		if len(asyncCalls) != asyncWaitCalls {
			tSuite.Fatalf("The 'cleanupChecks' method is not calling the 'wait' method once for each 'asyncCalls' element")
		}
	})
	tSuite.Run("The 'calls' method should return a slice that appends the elements of the 'asyncCalls' slice to the 'syncCalls' slice", func(tSuite *internal.TestSuite) {
		mock := &mockFn{}
		mock.syncCalls = []MockedCall{
			&mockMockedCall{}, &mockMockedCall{}, &mockMockedCall{},
			&mockMockedCall{},
			&mockMockedCall{}, &mockMockedCall{},
		}
		mock.asyncCalls = []MockedCall{
			&mockMockedCall{}, &mockMockedCall{},
			&mockMockedCall{}, &mockMockedCall{},
			&mockMockedCall{}, &mockMockedCall{}, &mockMockedCall{},
		}

		calls := mock.calls()
		if !reflect.DeepEqual(calls, append(mock.syncCalls, mock.asyncCalls...)) {
			tSuite.Fatalf("The 'calls' method is not returning the expected slice")
		}
	})
}
