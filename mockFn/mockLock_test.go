package mockFn

import "time"

type mockMutex struct {
	lockCalls   []time.Time
	unlockCalls []time.Time
}

func (m *mockMutex) Lock()   { m.lockCalls = append(m.lockCalls, time.Now()) }
func (m *mockMutex) Unlock() { m.unlockCalls = append(m.unlockCalls, time.Now()) }
