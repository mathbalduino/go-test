package mockFn

import "gitlab.com/loxe-tools/go-test/internal/mockCall"

type MockedCalls interface {
	calls() []MockedCall
}
type MockedCall interface {
	wait()
	matchArgs(...interface{}) bool
	expectedArgs() []interface{}
	call(...interface{}) []interface{}
	called() bool
	mockCall.MockedCall
}
type mockedCall struct {
	iMockCall
}

func (m *mockedCall) wait() {
	m.iMockCall.Wait()
}
func (m *mockedCall) matchArgs(i ...interface{}) bool {
	return m.iMockCall.MatchArgs(i...)
}
func (m *mockedCall) expectedArgs() []interface{} {
	return m.iMockCall.ExpectedArgs()
}
func (m *mockedCall) call(i ...interface{}) []interface{} {
	return m.iMockCall.Call(i...)
}
func (m *mockedCall) called() bool {
	return m.iMockCall.Called()
}
